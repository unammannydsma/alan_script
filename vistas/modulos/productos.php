<!--=====================================
BANNER
======================================-->

<?php

$servidor = Ruta::ctrRutaServidor();
$url = Ruta::ctrRuta();

$ruta = $rutas[0];

$banner = ControladorProductos::ctrMostrarBanner($ruta);

date_default_timezone_set('America/Bogota');

$fecha = date('Y-m-d');
$hora = date('H:i:s');

$fechaActual = $fecha.' '.$hora;

if($banner != null){

	if($banner["estado"] != 0){

		echo '<figure class="banner">

				<img src="'.$servidor.$banner["img"].'" class="img-responsive" width="100%">';	

				if($banner["ruta"] != "sin-categoria"){

					/*=============================================
					BANNER PARA CATEGORÍAS
					=============================================*/

					if($banner["tipo"] == "categorias"){

						$item = "ruta";
						$valor = $banner["ruta"];

						$ofertas = ControladorProductos::ctrMostrarCategorias($item, $valor);

						if($ofertas["oferta"] == 1){

							echo '<div class="textoBanner textoIzq">

								<h1 style="color:#fff" class="text-uppercase">'.$ofertas["categoria"].'</h1>

							</div>

							<div class="textoBanner textoDer">
							
								<h1 style="color:#fff">OFERTAS ESPECIALES</h1>';

								if($ofertas["precioOferta"] != 0){
									
									echo '<h2 style="color:#fff"><strong>Todos los productos a $ '.$ofertas["precioOferta"].'</strong></h2>';

								}

								if($ofertas["descuentoOferta"] != 0){
								
									echo '<h2 style="color:#fff"><strong>Todos los productos con '.$ofertas["descuentoOferta"].'% OFF</strong></h2>';
								}

							echo '<h3 class="col-md-0 col-sm-0 col-xs-0" style="color:#fff">
								
								La oferta termina en<br>

								<div class="countdown2" finOferta="'.$ofertas["finOferta"].'">


							</h3>';

							$datetime1 = new DateTime($ofertas["finOferta"]);
							$datetime2 = new DateTime($fechaActual);

							$interval = date_diff($datetime1, $datetime2);

							$finOferta = $interval->format('%a');

							if($finOferta == 0){

								echo '<h3 class="col-lg-0" style="color:#fff">La oferta termina hoy</h3>';

							}else if($finOferta == 1){
		
								echo '<h3 class="col-lg-0" style="color:#fff">La oferta termina en '.$finOferta.' día</h3>';
		
							}else{
		
								echo '<h3 class="col-lg-0" style="color:#fff">La oferta termina en '.$finOferta.' días</h3>';
		
							}


							echo '</div>';

						}

					}

					/*=============================================
					BANNER PARA SUBCATEGORÍAS
					=============================================*/

					if($banner["tipo"] == "subcategorias"){

						$item = "ruta";
						$valor = $banner["ruta"];

						$ofertas = ControladorProductos::ctrMostrarSubCategorias($item, $valor);

						if($ofertas[0]["oferta"] == 1){

							echo '<div class="textoBanner textoIzq">

					
								<h1 style="color:#fff" class="text-uppercase">'.$ofertas[0]["subcategoria"].'</h1>

							</div>

							<div class="textoBanner textoDer">
							
								<h1 style="color:#fff">OFERTAS ESPECIALES</h1>';

								if($ofertas[0]["precioOferta"] != 0){
									
									echo '<h2 style="color:#fff"><strong>Todos los productos a $ '.$ofertas[0]["precioOferta"].'</strong></h2>';

								}

								if($ofertas[0]["descuentoOferta"] != 0){
								
									echo '<h2 style="color:#fff"><strong>Todos los productos con '.$ofertas[0]["descuentoOferta"].'% OFF</strong></h2>';
								}

							echo '<h3 class="col-md-0 col-sm-0 col-xs-0" style="color:#fff">
								
								La oferta termina en<br>

								<div class="countdown2" finOferta="'.$ofertas[0]["finOferta"].'">


							</h3>';

							$datetime1 = new DateTime($ofertas[0]["finOferta"]);
							$datetime2 = new DateTime($fechaActual);

							$interval = date_diff($datetime1, $datetime2);

							$finOferta = $interval->format('%a');

							if($finOferta == 0){

								echo '<h3 class="col-lg-0" style="color:#fff">La oferta termina hoy</h3>';

							}else if($finOferta == 1){
		
								echo '<h3 class="col-lg-0" style="color:#fff">La oferta termina en '.$finOferta.' día</h3>';
		
							}else{
		
								echo '<h3 class="col-lg-0" style="color:#fff">La oferta termina en '.$finOferta.' días</h3>';
		
							}


							echo '</div>';

						}




						
					}



				}

		echo '</figure>';

	}

}

?>

<!--=====================================
BARRA PRODUCTOS
======================================-->

<div class="container-fluid well well-sm barraProductos">

	<div class="container">
		
		<div class="row">

			<div class="col-sm-6 col-xs-12">
				
				<div class="btn-group">
					
					 <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">

					  Ordenar Productos <span class="caret"></span></button>

					  <ul class="dropdown-menu" role="menu">

					  <?php
					  	
						echo '<li><a href="'.$url.$rutas[0].'/1/recientes">Más reciente</a></li>
							  <li><a href="'.$url.$rutas[0].'/1/antiguos">Más antiguo</a></li>';

						?>

					  </ul>

				</div>

			</div>
			
			<div class="col-sm-6 col-xs-12 organizarProductos">

				<div class="btn-group pull-right">

					 <button type="button" class="btn btn-default btnGrid" id="btnGrid0">
					 	
						<i class="fa fa-th" aria-hidden="true"></i>  

						<span class="col-xs-0 pull-right"> GRID</span>

					 </button>

					 <button type="button" class="btn btn-default btnList" id="btnList0">
					 	
						<i class="fa fa-list" aria-hidden="true"></i> 

						<span class="col-xs-0 pull-right"> VER CARACTERISICAS </span>

					 </button>
					
				</div>		

			</div>

		</div>

	</div>

</div>

<!--=====================================
LISTAR PRODUCTOS
======================================-->

<div class="container-fluid productos">

	<div class="container">
		
		<div class="row">

			<!--=====================================
			BREADCRUMB O MIGAS DE PAN
			======================================-->

			<ul class="breadcrumb fondoBreadcrumb lead text-uppercase">
				
				<li><a href="<?php echo $url;  ?>">INICIO</a></li>
				<li class="active pagActiva"><?php echo $rutas[0] ?></li>

			</ul>

			<?php

			/*=============================================
			LLAMADO DE PAGINACIÓN
			=============================================*/

			if(isset($rutas[1]) && preg_match('/^[0-9]+$/', $rutas[1])){

				if(isset($rutas[2])){

					if($rutas[2] == "antiguos"){

						$modo = "ASC";
						$_SESSION["ordenar"] = "ASC";

					}else{

						$modo = "DESC";
						$_SESSION["ordenar"] = "DESC";

					}

				}else{

					if(isset($_SESSION["ordenar"])){

						$modo = $_SESSION["ordenar"];

					}else{

						$modo = "DESC";

					}		

				}

				$base = ($rutas[1] - 1)*12;
				$tope = 12;

			}else{

				$rutas[1] = 1;
				$base = 0;
				$tope = 12;
				$modo = "DESC";
				$_SESSION["ordenar"] = "DESC";

			}

			/*=============================================
			LLAMADO DE PRODUCTOS DE CATEGORÍAS, SUBCATEGORÍAS Y DESTACADOS
			=============================================*/

			if($rutas[0] == "articulos-gratis"){

				$item2 = "precio";
				$valor2 = 0;
				$ordenar = "id";

			}else if($rutas[0] == "lo-mas-vendido"){

				$item2 = null;
				$valor2 = null;
				$ordenar = "ventas";

			}else if($rutas[0] == "lo-mas-visto"){

				$item2 = null;
				$valor2 = null;
				$ordenar = "vistas";

			}else{

				$ordenar = "id";
				$item1 = "ruta";
				$valor1 = $rutas[0];

				$categoria = ControladorProductos::ctrMostrarCategorias($item1, $valor1);

				if(!$categoria){

					$subCategoria = ControladorProductos::ctrMostrarSubCategorias($item1, $valor1);

					$item2 = "id_subcategoria";
					$valor2 = $subCategoria[0]["id"];

				}else{

					$item2 = "id_categoria";
					$valor2 = $categoria["id"];

				}
			}	

			

			$productos = ControladorProductos::ctrMostrarProductos($ordenar, $item2, $valor2, $base, $tope, $modo);
			$listaProductos = ControladorProductos::ctrListarProductos($ordenar, $item2, $valor2);


			if(!$productos){

				echo '<div class="col-xs-12 error404 text-center">

						 <h1><small>¡Oops!</small></h1>

						 <h2>Aún no hay productos en esta sección</h2>

					</div>';

			}else{

				echo '<ul class="grid0">';

					foreach ($productos as $key => $value) {

					if($value["estado"] != 0){
					
						echo '<li class="col-md-3 col-sm-6 col-xs-12">

							<figure>
								
								<a href="'.$url.$value["ruta"].'" class="pixelProducto">
									
									<img src="'.$servidor.$value["portada"].'" class="img-responsive">

								</a>

							</figure>

							<h4>
					
								<small>
									
									<a href="'.$url.$value["ruta"].'" class="pixelProducto">
										
										'.$value["titulo"].'<br>

										<span style="color:rgba(0,0,0,0)">-</span>';

										$fecha = date('Y-m-d');
										$fechaActual = strtotime('-30 day', strtotime($fecha));
										$fechaNueva = date('Y-m-d', $fechaActual);

										if($fechaNueva < $value["fecha"]){

											echo '<span class="label label-warning fontSize">Nuevo</span> ';

										}

										if($value["oferta"] != 0 && $value["precio"] != 0){

											echo '<span class="label label-warning fontSize">'.$value["descuentoOferta"].'% off</span>';

										}

									echo '</a>	

								</small>			

							</h4>

							<div class="col-xs-6 precio">';

							if($value["precio"] == 0){

								echo '<h2><small></small></h2>';

							}else{

								if($value["oferta"] != 0){

									echo '<h2>

											<small>
						
												<strong class="oferta">USD $'.$value["precio"].'</strong>

											</small>

											<small>$'.$value["precioOferta"].'</small>
										
										</h2>';

								}else{

									echo '<h2><small>USD $'.$value["precio"].'</small></h2>';

								}
								
							}
											
							echo '</div>

							<div class="col-xs-6 enlaces">
								
								<div class="btn-group pull-right">
									
									<button type="button" class="btn btn-default btn-xs deseos" idProducto="'.$value["id"].'" data-toggle="tooltip" title="Agregar y comparar a mi lista de deseos ">
										
										<i class="fa fa-star" aria-hidden="true"></i>

									</button>';

									if($value["tipo"] == "virtual" && $value["precio"] != 0){

										if($value["oferta"] != 0){

											echo '<button type="button" class="btn btn-default btn-xs agregarCarrito"  idProducto="'.$value["id"].'" imagen="'.$servidor.$value["portada"].'" titulo="'.$value["titulo"].'" precio="'.$value["precioOferta"].'" tipo="'.$value["tipo"].'" peso="'.$value["peso"].'" data-toggle="tooltip" title="Agregar al carrito de compras">

											<i class="fa fa-shopping-cart" aria-hidden="true"></i>

											</button>';

										}else{

											echo '<button type="button" class="btn btn-default btn-xs agregarCarrito"  idProducto="'.$value["id"].'" imagen="'.$servidor.$value["portada"].'" titulo="'.$value["titulo"].'" precio="'.$value["precio"].'" tipo="'.$value["tipo"].'" peso="'.$value["peso"].'" data-toggle="tooltip" title="Agregar al carrito de compras">

											<i class="fa fa-shopping-cart" aria-hidden="true"></i>

											</button>';

										}

									}

									echo '<a href="'.$url.$value["ruta"].'" class="pixelProducto">
									
										<button type="button" class="btn btn-default btn-xs" data-toggle="tooltip" title="Ver producto">
											
											<i class="fa fa-eye" aria-hidden="true"></i>

										</button>	
									
									</a>

								</div>

							</div>

						</li>';

					}
				}

				echo '</ul>

				<ul class="list0" style="display:none">';

				foreach ($productos as $key => $value) {

					if($value["estado"] != 0){

						echo '<li class="col-xs-12">
					  
				  		<div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
							   
							<figure>
						
								<a href="'.$url.$value["ruta"].'" class="pixelProducto">
									
									<img src="'.$servidor.$value["portada"].'" class="img-responsive">

								</a>

							</figure>

					  	</div>  	
							  
		
							
							<h1>

								<small>
		
									<a href="'.$url.$value["ruta"].'" class="pixelProducto">
										
										'.$value["titulo"].'<br>';

										$fecha = date('Y-m-d');
										$fechaActual = strtotime('-30 day', strtotime($fecha));
										$fechaNueva = date('Y-m-d', $fechaActual);

										if($fechaNueva < $value["fecha"]){

											echo '<span class="label label-warning">Nuevo</span> ';

										}

										if($value["oferta"] != 0 && $value["precio"] != 0){

											echo '<span class="label label-warning">'.$value["descuentoOferta"].'% off</span>';

										}		

									echo '</a>

								</small>

							</h1>

							<p class="text-muted">'.$value["titular"].'</p>';

							



//catalogo caracteristicas listas


				









				if($value["tipo"] == "fisico"){

					echo '<div class="col-md-12 col-sm-12 col-xs-12">';

                     echo'                           		
<style>
					<style>
table { 
  width: 100%; 
  border-collapse: collapse; 
  text-align:center;
}
/* Zebra striping */
tr:nth-of-type(odd) { 
  background: #ffcccc; 
}
th { 
  background: #364f93; 
  color: white; 
  font-weight: bold; 
}
td, th { 
  padding: 6px; 
  border: 1px solid #ccc; 
  text-align: center; 
  font-weight: bold; 
}

</style>
</head>
<body>


<div style="overflow-x:auto;">
  <table>
    <tr>
      <td></td>

         <th>NAAB CODE</th>
      <th>REG NAME</th>
      <th>CED</th>
      <th>BW</th>
      <th>WW</th>
      <th>YW</th>
      <th>DMI</th>

      <th>MILK</th>
      <th>TM</th>
      <th>CEM</th>
      <th>SC</th>
      <th>REA</th>
      <th>IMF</th>
      <th>FAT</th>
  
      <th>CW</th>
      <th>YH</th>
      <th>HP</th>
      <th>MCW</th>
      <th>MARB</th>
      <th>BMI</th>
      <th>BII</th>
      <th>CHB</th>
      <th>SIRE NAME</th>

    </tr>
    <tr>
       <th></th>
      <td >'.$value["naab code fisicoproducto"].'</td>
      <td>'.$value["regname"].'</td>
      <td>'.$value["ced"].'</td>
      <td>'.$value["bw"].'</td>
      <td>'.$value["ww"].'</td>
      <td>'.$value["yw"].'</td>
      <td>'.$value["dmi"].'</td>
      <td>'.$value["milk"].'</td>
      <td>'.$value["tm"].'</td>
      <td>'.$value["cem"].'</td>
      <td>'.$value["sc"].'</td>
      <td>'.$value["rea"].'</td>
      <td>'.$value["imf"].'</td>
      <td>'.$value["fatt"].'</td>
      <td>'.$value["cwfisico"].'</td>
      <td>'.$value["yh"].'</td>
      <td>'.$value["hpfisico"].'</td>
       <td>'.$value["mcw"].'</td>
        <td>'.$value["marb"].'</td>
         <td>'.$value["bmi"].'</td>
          <td>'.$value["bii"].'</td>
           <td>'.$value["chb"].'</td>
            <td>'.$value["sirename fisico"].'</td>
        

    </tr>
 
  
  </table>
</div>

<br><br>

<hr>';






				}else if($value["tipo"] == "virtual"){

	




					echo '<div class="col-xs-12">
					<style>
table { 
  width: 100%; 
  border-collapse: collapse; 
  text-align:center;
}
/* Zebra striping */
tr:nth-of-type(odd) { 
  background: #ffcccc; 
}
th { 
  background: #364f93; 
  color: white; 
  font-weight: bold; 
}
td, th { 
  padding: 6px; 
  border: 1px solid #ccc; 
  text-align: center; 
  font-weight: bold; 
}

</style>
</head>
<body>


<div style="overflow-x:auto;">
  <table>


	<div class="col-lg-10 col-md-7 col-sm-8 col-xs-12">

	<small>
    <table id="table_id" class="table table-bordered table-striped table-hover">
        <thead>
            <tr>
                <th>Naab Code</th>
                <th>Name</th>
                <th>Reg Name</th>
                <th>A2A2t</th>
                <th>Birth Date</th>
                <th>Obs</th>
                <th>RHA</th>
                <th>Eco Feed</th>
                <th>TPI</th>
                <th>JPI</th>
                <th>PPR</th>
                <th>Net Merit</th>
                <th>$CM</th>
                <th>Grazing Merit</th>
                <th>PTA Milk</th>
                <th>PTA fat</th>
                <th>%fat</th>
                <th>PTA Pro</th>
                <th> % Pro</th>
                <th>CFP</th>
                <th> Milk Rel</th>
                <th>SCS</th>
                <th>PL</th>
                <th>PTA DPR</th>
                <th>PTA LIV</th>
                <th>Fertil Index</th>
                <th>PTA Type</th>
                <th>UDC</th>
                <th>JUI</th>
                <th>MO</th>
                <th>FLC</th>
                <th>SCE</th>
                <th>DLE</th>
                <th>SSB</th>
                <th>DSB</th>
                <th>SIRE</th>
                <th>MGS</th>
                <th>MGGS</th>
                <th>AAa</th>
                <th>DMS</th>


            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="text-center">'.$value["nabcode"].'</td>
                <td>'.$value["name"].'</td>
                <td>'.$value["regnamee"].'</td>
                <td class="text-right">'.$value["a2a2t"].'</td>
                <td class="text-center">'.$value["birthdate"].'</td>
                <td class="text-center">'.$value["obs"].'</td>
                <td class="text-center">'.$value["RHA"].'</td>
                <td class="text-center">'.$value["ecofeed"].'</td>
                <td class="text-center">'.$value["tpii"].'</td>
                <td class="text-center">'.$value["jpi"].'</td>
                <td class="text-center">'.$value["ppr"].'</td>
                <td class="text-center">'.$value["netmerito"].'</td>
                <td class="text-center">'.$value["productocm"].'</td>
                <td class="text-center">'.$value["grazingmerito"].'</td>
                <td class="text-center">'.$value["ptamilk"].'</td>
                <td class="text-center">'.$value["ptafat"].'</td>
                <td class="text-center">'.$value["porcentfat"].'</td>
                <td class="text-center">'.$value["ptapro"].'</td>
                <td class="text-center">'.$value["porcentpro"].'</td>
                <td class="text-center">'.$value["cfp"].'</td>
                <td class="text-center">'.$value["milkrel"].'</td>
                <td class="text-center">'.$value["productoscs"].'</td>
                <td class="text-center">'.$value["productopl"].'</td>
                <td class="text-center">'.$value["ptadpr"].'</td>
                <td class="text-center">'.$value["ptaliv"].'</td>
                <td class="text-center">'.$value["fertilindex"].'</td>
                <td class="text-center">'.$value["ptatype"].'</td>
                <td class="text-center">'.$value["udc"].'</td>
                <td class="text-center">'.$value["jui"].'</td>
                <td class="text-center">'.$value["moo"].'</td>
                <td class="text-center">'.$value["flc"].'</td>
                <td class="text-center">'.$value["sce"].'</td>
                <td class="text-center">'.$value["dle"].'</td>
                <td class="text-center">'.$value["ssb"].'</td>
                <td class="text-center">'.$value["dsb"].'</td>
                <td class="text-center">'.$value["sire"].'</td>
                <td class="text-center">'.$value["mgs"].'</td>
                <td class="text-center">'.$value["mggs"].'</td>
                <td class="text-center">'.$value["aAa"].'</td>
                <td class="text-center">'.$value["dms"].'</td>

            </tr>

        </tbody>
        <tfood>
            <tr>
               
            </tr>
        </tfood>
    </table>


<br><br>








';



	}if($value["tipo"] == "bufalo"){


echo'

<div class="col-xs-12">
					<style>
table { 
  width: 100%; 
  border-collapse: collapse; 
  text-align:center;
}
/* Zebra striping */
tr:nth-of-type(odd) { 
  background: #ffcccc; 
}
th { 
  background: #364f93; 
  color: white; 
  font-weight: bold; 
}
td, th { 
  padding: 6px; 
  border: 1px solid #ccc; 
  text-align: center; 
  font-weight: bold; 
}

</style>
</head>
<body>


<div style="overflow-x:auto;">
  <table>


	<div class="col-lg-10 col-md-7 col-sm-8 col-xs-12">

	<small>
    <table id="table_id" class="table table-bordered table-striped table-hover">
        <thead>
            <tr>
                <th>CODE</th>
                <th>Nombre</th>
                <th>IMQ</th>
                <th>Hijas</th>
                <th>Hatos</th>
                <th>Leche (kg)</th>
                <th>Proteina (kg)</th>
                <th>Proteina (%)</th>
                <th>Grasa (kg)</th>
                <th>Grasa (%)</th>
             


            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="text-center">'.$value["bufalo code"].'</td>
                <td>'.$value["bufalo nombre"].'</td>
                <td>'.$value["bufalo imq"].'</td>
                <td>'.$value["bufalo hijas"].'</td>
                <td class="text-center">'.$value["bufalo hatos"].'</td>
                <td class="text-center">'.$value["bufalo leche"].'</td>
                <td class="text-center">'.$value["bufalo proteina kg"].'</td>
                <td class="text-center">'.$value["bufalo proteina %"].'</td>
                <td class="text-center">'.$value["bufalo grasa kg"].'</td>
                <td class="text-center">'.$value["bufalo grasa %"].'</td>
                
          
          
       

            </tr>

        </tbody>
        <tfood>
            <tr>
               
            </tr>
        </tfood>
    </table>


<br><br>









';




	}

		





							if($value["precio"] == 0){

								echo '<h2><small></small></h2>';

							}else{

								if($value["oferta"] != 0){

									echo '<h2>

											<small>
						
												<strong class="oferta">USD $'.$value["precio"].'</strong>

											</small>

											<small>$'.$value["precioOferta"].'</small>
										
										</h2>';

								}else{

									echo '<h2><small>USD $'.$value["precio"].'</small></h2>';

								}
								
							}

							echo '<div class="btn-group pull-left enlaces">
						  	
						  		<button type="button" class="btn btn-default btn-xs deseos"  idProducto="'.$value["id"].'" data-toggle="tooltip" title="Agregar y comparar a mi lista de deseos">

						  			<i class="fa fa-star" aria-hidden="true"></i>

						  		</button>';

						  		if($value["tipo"] == "virtual" && $value["precio"] != 0){

										if($value["oferta"] != 0){

											echo '<button type="button" class="btn btn-default btn-xs agregarCarrito"  idProducto="'.$value["id"].'" imagen="'.$servidor.$value["portada"].'" titulo="'.$value["titulo"].'" precio="'.$value["precioOferta"].'" tipo="'.$value["tipo"].'" peso="'.$value["peso"].'" data-toggle="tooltip" title="Agregar al carrito de compras">

											<i class="fa fa-shopping-cart" aria-hidden="true"></i>

											</button>';

										}else{

											echo '<button type="button" class="btn btn-default btn-xs agregarCarrito"  idProducto="'.$value["id"].'" imagen="'.$servidor.$value["portada"].'" titulo="'.$value["titulo"].'" precio="'.$value["precio"].'" tipo="'.$value["tipo"].'" peso="'.$value["peso"].'" data-toggle="tooltip" title="Agregar al carrito de compras">

											<i class="fa fa-shopping-cart" aria-hidden="true"></i>

											</button>';

										}

									}

						  		echo '<a href="'.$url.$value["ruta"].'" class="pixelProducto">

							  		<button type="button" class="btn btn-default btn-xs" data-toggle="tooltip" title="Ver producto">

							  		<i class="fa fa-eye" aria-hidden="true"></i>

							  		</button>

						  		</a>
							
							</div>

						</div>

						<div class="col-xs-12"><hr></div>

					</li>';

					}

				}

				echo '</ul>';
			}

			?>

			<div class="clearfix"></div>

			<center>

			<!--=====================================
			PAGINACIÓN
			======================================-->
			
			<?php

				if(count($listaProductos) != 0){

					$pagProductos = ceil(count($listaProductos)/12);

					if($pagProductos > 4){

						/*=============================================
						LOS BOTONES DE LAS PRIMERAS 4 PÁGINAS Y LA ÚLTIMA PÁG
						=============================================*/

						if($rutas[1] == 1){

							echo '<ul class="pagination">';
							
							for($i = 1; $i <= 4; $i ++){

								echo '<li id="item'.$i.'"><a href="'.$url.$rutas[0].'/'.$i.'">'.$i.'</a></li>';

							}

							echo ' <li class="disabled"><a>...</a></li>
								   <li id="item'.$pagProductos.'"><a href="'.$url.$rutas[0].'/'.$pagProductos.'">'.$pagProductos.'</a></li>
								   <li><a href="'.$url.$rutas[0].'/2"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>

							</ul>';

						}

						/*=============================================
						LOS BOTONES DE LA MITAD DE PÁGINAS HACIA ABAJO
						=============================================*/

						else if($rutas[1] != $pagProductos && 
							    $rutas[1] != 1 &&
							    $rutas[1] <  ($pagProductos/2) &&
							    $rutas[1] < ($pagProductos-3)
							    ){

								$numPagActual = $rutas[1];

								echo '<ul class="pagination">
									  <li><a href="'.$url.$rutas[0].'/'.($numPagActual-1).'"><i class="fa fa-angle-left" aria-hidden="true"></i></a></li> ';
							
								for($i = $numPagActual; $i <= ($numPagActual+3); $i ++){

									echo '<li id="item'.$i.'"><a href="'.$url.$rutas[0].'/'.$i.'">'.$i.'</a></li>';

								}

								echo ' <li class="disabled"><a>...</a></li>
									   <li id="item'.$pagProductos.'"><a href="'.$url.$rutas[0].'/'.$pagProductos.'">'.$pagProductos.'</a></li>
									   <li><a href="'.$url.$rutas[0].'/'.($numPagActual+1).'"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>

								</ul>';

						}

						/*=============================================
						LOS BOTONES DE LA MITAD DE PÁGINAS HACIA ARRIBA
						=============================================*/

						else if($rutas[1] != $pagProductos && 
							    $rutas[1] != 1 &&
							    $rutas[1] >=  ($pagProductos/2) &&
							    $rutas[1] < ($pagProductos-3)
							    ){

								$numPagActual = $rutas[1];
							
								echo '<ul class="pagination">
								   <li><a href="'.$url.$rutas[0].'/'.($numPagActual-1).'"><i class="fa fa-angle-left" aria-hidden="true"></i></a></li> 
								   <li id="item1"><a href="'.$url.$rutas[0].'/1">1</a></li>
								   <li class="disabled"><a>...</a></li>
								';
							
								for($i = $numPagActual; $i <= ($numPagActual+3); $i ++){

									echo '<li id="item'.$i.'"><a href="'.$url.$rutas[0].'/'.$i.'">'.$i.'</a></li>';

								}


								echo '  <li><a href="'.$url.$rutas[0].'/'.($numPagActual+1).'"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
									</ul>';
						}

						/*=============================================
						LOS BOTONES DE LAS ÚLTIMAS 4 PÁGINAS Y LA PRIMERA PÁG
						=============================================*/

						else{

							$numPagActual = $rutas[1];

							echo '<ul class="pagination">
								   <li><a href="'.$url.$rutas[0].'/'.($numPagActual-1).'"><i class="fa fa-angle-left" aria-hidden="true"></i></a></li> 
								   <li id="item1"><a href="'.$url.$rutas[0].'/1">1</a></li>
								   <li class="disabled"><a>...</a></li>
								';
							
							for($i = ($pagProductos-3); $i <= $pagProductos; $i ++){

								echo '<li id="item'.$i.'"><a href="'.$url.$rutas[0].'/'.$i.'">'.$i.'</a></li>';

							}

							echo ' </ul>';

						}

					}else{

						echo '<ul class="pagination">';
						
						for($i = 1; $i <= $pagProductos; $i ++){

							echo '<li id="item'.$i.'"><a href="'.$url.$rutas[0].'/'.$i.'">'.$i.'</a></li>';

						}

						echo '</ul>';

					}

				}

			?>

			</center>

</div>

</div>

</div>