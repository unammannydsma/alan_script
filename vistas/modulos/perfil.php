<!--=====================================
VALIDAR SESIÓN
======================================-->

<?php

$url = Ruta::ctrRuta();
$servidor = Ruta::ctrRutaServidor();

if(!isset($_SESSION["validarSesion"])){

	echo '<script>
	
		window.location = "'.$url.'";

	</script>';

	exit();

}

?>

<!--=====================================
BREADCRUMB PERFIL
======================================-->

<div class="container-fluid well well-sm">
	
	<div class="container">
		
		<div class="row">
			
			<ul class="breadcrumb fondoBreadcrumb text-uppercase">
				
				<li><a href="<?php echo $url;  ?>">INICIO</a></li>
				<li class="active pagActiva"><?php echo $rutas[0] ?></li>

			</ul>

		</div>

	</div>

</div>

<!--=====================================
SECCIÓN PERFIL
======================================-->

<div class="container-fluid">

	<div class="container">

		<ul class="nav nav-tabs">
		  
	  		<li class="active">	  			
			  	<a data-toggle="tab" href="#compras">
			  	<i class="fa fa-list-ul"></i> CONTACTANOS</a>
	  		</li>

	  		<li> 				
		  		<a data-toggle="tab" href="#deseos">
		  		<i class="fa fa-gift"></i> MI LISTA DE DESEOS</a>
	  		</li>

	  		<li>				
	  			<a data-toggle="tab" href="#perfil">
	  			<i class="fa fa-user"></i> EDITAR PERFIL</a>
	  		</li>

	  		<li>				
		 	 	<a href="<?php echo $url; ?>ofertas">
		 	 	<i class="fa fa-star"></i>	VER OFERTAS</a>
	  		</li>
		
		</ul>

		<div class="tab-content">

			<!--=====================================
			PESTAÑA COMPRAS
			======================================-->

	  		<div id="compras" class="tab-pane fade in active">
		    
				<div class="panel-group">

				<?php

					$item = "id_usuario";
					$valor = $_SESSION["id"];

					$compras = ControladorUsuarios::ctrMostrarCompras($item, $valor);

					if(!$compras){

						echo '<div class="col-xs-12 text-center error404">
				               
				    		<h1><small>¡Oops!</small></h1>
				    
				    		<h2>Aún no tienes compras realizadas en esta tienda</h2>

				  		</div>';

					}else{

						foreach ($compras as $key => $value1) {

							$ordenar = "id";
							$item = "id";
							$valor = $value1["id_producto"];

							$productos = ControladorProductos::ctrListarProductos($ordenar, $item, $valor);

							foreach ($productos as $key => $value2) {
							
								echo '<div class="panel panel-default">
									    
									    <div class="panel-body">

											<div class="col-md-2 col-sm-6 col-xs-12">

												<figure>
												
													<img class="img-thumbnail" src="'.$servidor.$value2["portada"].'">

												</figure>

											</div>

											<div class="col-sm-6 col-xs-12">

												<h1><small>'.$value2["titulo"].'</small></h1>

												<p>'.$value2["titular"].'</p>';

												if($value2["tipo"] == "virtual"){


												}else{

													echo '<h6>Proceso de entrega: '.$value2["entrega"].' días hábiles</h6>';

													if($value1["envio"] == 0){

														echo '<div class="progress">

															<div class="progress-bar progress-bar-info" role="progressbar" style="width:33.33%">
																<i class="fa fa-check"></i> Despachado
															</div>

															<div class="progress-bar progress-bar-default" role="progressbar" style="width:33.33%">
																<i class="fa fa-clock-o"></i> Enviando
															</div>

															<div class="progress-bar progress-bar-success" role="progressbar" style="width:33.33%">
																<i class="fa fa-clock-o"></i> Entregado
															</div>

														</div>';

													}

													if($value1["envio"] == 1){

														echo '<div class="progress">

															<div class="progress-bar progress-bar-info" role="progressbar" style="width:33.33%">
																<i class="fa fa-check"></i> Despachado
															</div>

															<div class="progress-bar progress-bar-default" role="progressbar" style="width:33.33%">
																<i class="fa fa-check"></i> Enviando
															</div>

															<div class="progress-bar progress-bar-success" role="progressbar" style="width:33.33%">
																<i class="fa fa-clock-o"></i> Entregado
															</div>

														</div>';

													}

													if($value1["envio"] == 2){

														echo '<div class="progress">

															<div class="progress-bar progress-bar-info" role="progressbar" style="width:33.33%">
																<i class="fa fa-check"></i> Despachado
															</div>

															<div class="progress-bar progress-bar-default" role="progressbar" style="width:33.33%">
																<i class="fa fa-check"></i> Enviando
															</div>

															<div class="progress-bar progress-bar-success" role="progressbar" style="width:33.33%">
																<i class="fa fa-check"></i> Entregado
															</div>

														</div>';

													}

												}

												echo '<h4 class="pull-right"><small>Comprado el '.substr($value1["fecha"],0,-8).'</small></h4>
																
											</div>

											<div class="col-md-4 col-xs-12">';

											$datos = array("idUsuario"=>$_SESSION["id"],
															"idProducto"=>$value2["id"] );

											$comentarios = ControladorUsuarios::ctrMostrarComentariosPerfil($datos);

												echo '<div class="pull-right">

													<a class="calificarProducto" href="#modalComentarios" data-toggle="modal" idComentario="'.$comentarios["id"].'">
													
														<button class="btn btn-default backColor">Dejanos tus datos</button>

													</a>

												</div>

												<br><br>

												<div class="pull-right">

													<h3 class="text-right">';

													if($comentarios["calificacion"] == 0 && $comentarios["comentario"] == ""){

														echo '<i class="fa fa-star-o text-success" aria-hidden="true"></i>
																<i class="fa fa-star-o text-success" aria-hidden="true"></i>
																<i class="fa fa-star-o text-success" aria-hidden="true"></i>
																<i class="fa fa-star-o text-success" aria-hidden="true"></i>
																<i class="fa fa-star-o text-success" aria-hidden="true"></i>';

													}else{

														switch($comentarios["calificacion"]){

															case 0.5:
															echo '<i class="fa fa-star-half-o text-success" aria-hidden="true"></i>
																  <i class="fa fa-star-o text-success" aria-hidden="true"></i>
																  <i class="fa fa-star-o text-success" aria-hidden="true"></i>
																  <i class="fa fa-star-o text-success" aria-hidden="true"></i>
																  <i class="fa fa-star-o text-success" aria-hidden="true"></i>';
															break;

															case 1.0:
															echo '<i class="fa fa-star text-success" aria-hidden="true"></i>
																  <i class="fa fa-star-o text-success" aria-hidden="true"></i>
																  <i class="fa fa-star-o text-success" aria-hidden="true"></i>
																  <i class="fa fa-star-o text-success" aria-hidden="true"></i>
																  <i class="fa fa-star-o text-success" aria-hidden="true"></i>';
															break;

															case 1.5:
															echo '<i class="fa fa-star text-success" aria-hidden="true"></i>
																  <i class="fa fa-star-half-o text-success" aria-hidden="true"></i>
																  <i class="fa fa-star-o text-success" aria-hidden="true"></i>
																  <i class="fa fa-star-o text-success" aria-hidden="true"></i>
																  <i class="fa fa-star-o text-success" aria-hidden="true"></i>';
															break;

															case 2.0:
															echo '<i class="fa fa-star text-success" aria-hidden="true"></i>
																  <i class="fa fa-star text-success" aria-hidden="true"></i>
																  <i class="fa fa-star-o text-success" aria-hidden="true"></i>
																  <i class="fa fa-star-o text-success" aria-hidden="true"></i>
																  <i class="fa fa-star-o text-success" aria-hidden="true"></i>';
															break;

															case 2.5:
															echo '<i class="fa fa-star text-success" aria-hidden="true"></i>
																  <i class="fa fa-star text-success" aria-hidden="true"></i>
																  <i class="fa fa-star-half-o text-success" aria-hidden="true"></i>
																  <i class="fa fa-star-o text-success" aria-hidden="true"></i>
																  <i class="fa fa-star-o text-success" aria-hidden="true"></i>';
															break;

															case 3.0:
															echo '<i class="fa fa-star text-success" aria-hidden="true"></i>
																  <i class="fa fa-star text-success" aria-hidden="true"></i>
																  <i class="fa fa-star text-success" aria-hidden="true"></i>
																  <i class="fa fa-star-o text-success" aria-hidden="true"></i>
																  <i class="fa fa-star-o text-success" aria-hidden="true"></i>';
															break;

															case 3.5:
															echo '<i class="fa fa-star text-success" aria-hidden="true"></i>
																  <i class="fa fa-star text-success" aria-hidden="true"></i>
																  <i class="fa fa-star text-success" aria-hidden="true"></i>
																  <i class="fa fa-star-half-o text-success" aria-hidden="true"></i>
																  <i class="fa fa-star-o text-success" aria-hidden="true"></i>';
															break;

															case 4.0:
															echo '<i class="fa fa-star text-success" aria-hidden="true"></i>
																  <i class="fa fa-star text-success" aria-hidden="true"></i>
																  <i class="fa fa-star text-success" aria-hidden="true"></i>
																  <i class="fa fa-star text-success" aria-hidden="true"></i>
																  <i class="fa fa-star-o text-success" aria-hidden="true"></i>';
															break;

															case 4.5:
															echo '<i class="fa fa-star text-success" aria-hidden="true"></i>
																  <i class="fa fa-star text-success" aria-hidden="true"></i>
																  <i class="fa fa-star text-success" aria-hidden="true"></i>
																  <i class="fa fa-star text-success" aria-hidden="true"></i>
																  <i class="fa fa-star-half-o text-success" aria-hidden="true"></i>';
															break;

															case 5.0:
															echo '<i class="fa fa-star text-success" aria-hidden="true"></i>
																  <i class="fa fa-star text-success" aria-hidden="true"></i>
																  <i class="fa fa-star text-success" aria-hidden="true"></i>
																  <i class="fa fa-star text-success" aria-hidden="true"></i>
																  <i class="fa fa-star text-success" aria-hidden="true"></i>';
															break;

														}


													}
												
														
													echo '</h3>

													<p class="panel panel-default text-right" style="padding:5px">

														<small>

														'.$comentarios["comentario"].'

														</small>
													
													</p>

												</div>

											</div>

									    </div>

									</div>';

							}
							
						}
					}
				?>
				  
				

				</div>

		  	</div>

		  	<!--=====================================
			PESTAÑA DESEOS
			======================================-->


               
		  	<div id="deseos" class="tab-pane fade">
                   <div class="imprimir"   style="text-align:right;">
                   	  <input type="button" value="Imprimir" onclick="javascript:window.print()" />


                   </div>
		    	

			<?php

				$item = $_SESSION["id"];

				$deseos = ControladorUsuarios::ctrMostrarDeseos($item);

				if(!$deseos){

					echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center error404">
				               
			    		<h1><small>¡Oops!</small></h1>
			    
			    		<h2>Aún no tiene productos en su lista de deseos</h2>

			  		</div>';
				
				}else{

					foreach ($deseos as $key => $value1) {

						$ordenar = "id";
						$valor = $value1["id_producto"];
						$item = "id";

						$productos = ControladorProductos::ctrListarProductos($ordenar, $item, $valor);

						echo '<ul class="grid0">';

							foreach ($productos as $key => $value2) {
							
						echo '<li class="col-xs-12">
					     <br>  <br>  <br>  <br>


				  		<div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                                 
							   
							<figure>
						       
								<a href="'.$value2["ruta"].'" class="pixelProducto">
									
									<img src="'.$servidor.$value2["portada"].'" class="img-responsive">

								</a>

							</figure>

					  	</div>  	
							  
						<div class="col-lg-10 col-md-7 col-sm-8 col-xs-12">
							
							<h1>

								<small>
		
									
										
										'.$value2["titulo"].'<br>';

										

									echo '</a>

								</small>

							</h1>

							<p class="text-muted">'.'</p>';

							


//catalogo caracteristicas listas

		

				if($value2["tipo"] == "fisico"){

					echo '<div class="col-md-12 col-sm-12 col-xs-12">';

                     echo'                           		
<style>
					<style>
table { 
  width: 100%; 
  border-collapse: collapse; 
  text-align:center;
}
/* Zebra striping */
tr:nth-of-type(odd) { 
  background: #ffcccc; 
}
th { 
  background: #364f93; 
  color: white; 
  font-weight: bold; 
}
td, th { 
  padding: 6px; 
  border: 1px solid #ccc; 
  text-align: center; 
  font-weight: bold; 
}

</style>
</head>
<body>


<div style="overflow-x:auto;">

<table>
    <tr>
      <td></td>

         <th>NAAB CODE</th>
      <th>REG NAME</th>
      <th>CED</th>
      <th>BW</th>
      <th>WW</th>
      <th>YW</th>
      <th>DMI</th>

      <th>MILK</th>
      <th>TM</th>
      <th>CEM</th>
      <th>SC</th>
      <th>REA</th>
      <th>IMF</th>
      <th>FAT</th>
  
      <th>CW</th>
      <th>YH</th>
      <th>HP</th>
      <th>MCW</th>
      <th>MARB</th>
      <th>BMI</th>
      <th>BII</th>
      <th>CHB</th>
      <th>SIRE NAME</th>

    </tr>
    <tr>
       <th></th>
      <td >'.$value2["naab code fisicoproducto"].'</td>
      <td>'.$value2["regname"].'</td>
      <td>'.$value2["ced"].'</td>
      <td>'.$value2["bw"].'</td>
      <td>'.$value2["ww"].'</td>
      <td>'.$value2["yw"].'</td>
      <td>'.$value2["dmi"].'</td>
      <td>'.$value2["milk"].'</td>
      <td>'.$value2["tm"].'</td>
      <td>'.$value2["cem"].'</td>
      <td>'.$value2["sc"].'</td>
      <td>'.$value2["rea"].'</td>
      <td>'.$value2["imf"].'</td>
      <td>'.$value2["fatt"].'</td>
      <td>'.$value2["cwfisico"].'</td>
      <td>'.$value2["yh"].'</td>
      <td>'.$value2["hpfisico"].'</td>
       <td>'.$value2["mcw"].'</td>
        <td>'.$value2["marb"].'</td>
         <td>'.$value2["bmi"].'</td>
          <td>'.$value2["bii"].'</td>
           <td>'.$value2["chb"].'</td>
            <td>'.$value2["sirename fisico"].'</td>
        

    </tr>
 
  
  </table>
  
</div>

<br><br>

<hr>';





	}else if($value2["tipo"] == "virtual"){


	




					echo '<div class="col-xs-12">
					<style>
table { 
  width: 100%; 
  border-collapse: collapse; 
  text-align:center;
}
/* Zebra striping */
tr:nth-of-type(odd) { 
  background: #ffcccc; 
}
th { 
  background: #364f93; 
  color: white; 
  font-weight: bold; 
}
td, th { 
  padding: 6px; 
  border: 1px solid #ccc; 
  text-align: center; 
  font-weight: bold; 
}

</style>
</head>
<body>


<div style="overflow-x:auto;">
<table id="table_id" class="table table-bordered table-striped table-hover">
        <thead>
            <tr>
                <th>Naab Code</th>
                <th>Name</th>
                <th>Reg Name</th>
                <th>A2A2t</th>
                <th>Birth Date</th>
                <th>Obs</th>
                <th>RHA</th>
                <th>Eco Feed</th>
                <th>TPI</th>
                <th>JPI</th>
                <th>PPR</th>
                <th>Net Merit</th>
                <th>$CM</th>
                <th>Grazing Merit</th>
                <th>PTA Milk</th>
                <th>PTA fat</th>
                <th>%fat</th>
                <th>PTA Pro</th>
                <th> % Pro</th>
                <th>CFP</th>
                <th> Milk Rel</th>
                <th>SCS</th>
                <th>PL</th>
                <th>PTA DPR</th>
                <th>PTA LIV</th>
                <th>Fertil Index</th>
                <th>PTA Type</th>
                <th>UDC</th>
                <th>JUI</th>
                <th>MO</th>
                <th>FLC</th>
                <th>SCE</th>
                <th>DLE</th>
                <th>SSB</th>
                <th>DSB</th>
                <th>SIRE</th>
                <th>MGS</th>
                <th>MGGS</th>
                <th>AAa</th>
                <th>DMS</th>


            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="text-center">'.$value2["nabcode"].'</td>
                <td>'.$value2["name"].'</td>
                <td>'.$value2["regnamee"].'</td>
                <td class="text-right">'.$value2["a2a2t"].'</td>
                <td class="text-center">'.$value2["birthdate"].'</td>
                <td class="text-center">'.$value2["obs"].'</td>
                <td class="text-center">'.$value2["RHA"].'</td>
                <td class="text-center">'.$value2["ecofeed"].'</td>
                <td class="text-center">'.$value2["tpii"].'</td>
                <td class="text-center">'.$value2["jpi"].'</td>
                <td class="text-center">'.$value2["ppr"].'</td>
                <td class="text-center">'.$value2["netmerito"].'</td>
                <td class="text-center">'.$value2["productocm"].'</td>
                <td class="text-center">'.$value2["grazingmerito"].'</td>
                <td class="text-center">'.$value2["ptamilk"].'</td>
                <td class="text-center">'.$value2["ptafat"].'</td>
                <td class="text-center">'.$value2["porcentfat"].'</td>
                <td class="text-center">'.$value2["ptapro"].'</td>
                <td class="text-center">'.$value2["porcentpro"].'</td>
                <td class="text-center">'.$value2["cfp"].'</td>
                <td class="text-center">'.$value2["milkrel"].'</td>
                <td class="text-center">'.$value2["productoscs"].'</td>
                <td class="text-center">'.$value2["productopl"].'</td>
                <td class="text-center">'.$value2["ptadpr"].'</td>
                <td class="text-center">'.$value2["ptaliv"].'</td>
                <td class="text-center">'.$value2["fertilindex"].'</td>
                <td class="text-center">'.$value2["ptatype"].'</td>
                <td class="text-center">'.$value2["udc"].'</td>
                <td class="text-center">'.$value2["jui"].'</td>
                <td class="text-center">'.$value2["moo"].'</td>
                <td class="text-center">'.$value2["flc"].'</td>
                <td class="text-center">'.$value2["sce"].'</td>
                <td class="text-center">'.$value2["dle"].'</td>
                <td class="text-center">'.$value2["ssb"].'</td>
                <td class="text-center">'.$value2["dsb"].'</td>
                <td class="text-center">'.$value2["sire"].'</td>
                <td class="text-center">'.$value2["mgs"].'</td>
                <td class="text-center">'.$value2["mggs"].'</td>
                <td class="text-center">'.$value2["aAa"].'</td>
                <td class="text-center">'.$value2["dms"].'</td>

            </tr>

        </tbody>
        <tfood>
            <tr>
               
            </tr>
        </tfood>
    </table>


<br><br>







';



}else{
  echo'
 <table id="table_id" class="table table-bordered table-striped table-hover">
        <thead>
            <tr>
                <th>CODE</th>
                <th>Nombre</th>
                <th>IMQ</th>
                <th>Hijas</th>
                <th>Hatos</th>
                <th>Leche (kg)</th>
                <th>Proteina (kg)</th>
                <th>Proteina (%)</th>
                <th>Grasa (kg)</th>
                <th>Grasa (%)</th>
             


            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="text-center">'.$value2["bufalo code"].'</td>
                <td>'.$value2["bufalo nombre"].'</td>
                <td>'.$value2["bufalo imq"].'</td>
                <td>'.$value2["bufalo hijas"].'</td>
                <td class="text-center">'.$value2["bufalo hatos"].'</td>
                <td class="text-center">'.$value2["bufalo leche"].'</td>
                <td class="text-center">'.$value2["bufalo proteina kg"].'</td>
                <td class="text-center">'.$value2["bufalo proteina %"].'</td>
                <td class="text-center">'.$value2["bufalo grasa kg"].'</td>
                <td class="text-center">'.$value2["bufalo grasa %"].'</td>
                
          
       

            </tr>

        </tbody>
        <tfood>
            <tr>
               
            </tr>
        </tfood>
    </table>


<br><br>




  ';


}









							echo '</div>

									<div class="col-xs-6 enlaces">
										
										<div class="btn-group pull-right">
											
											<button type="button" class="btn btn-danger btn-xs quitarDeseo" idDeseo="'.$value1["id"].'" data-toggle="tooltip" title="Quitar de mi lista de deseos">
												
												<i class="fa fa-star" aria-hidden="true"></i>

											</button>';
						  		if($value2["tipo"] == "virtual" && $value2["precio"] != 0){

										if($value2["oferta"] != 0){

											echo '<button type="button" class="btn btn-default btn-xs agregarCarrito"  idProducto="'.$value2["id"].'" imagen="'.$servidor.$value2["portada"].'" titulo="'.$value2["titulo"].'" precio="'.$value2["precioOferta"].'" tipo="'.$value2["tipo"].'" peso="'.$value2["peso"].'" data-toggle="tooltip" title="Agregar al carrito de compras">

											<i class="fa fa-shopping-cart" aria-hidden="true"></i>

											</button>';

										}else{

											echo '<button type="button" class="btn btn-default btn-xs agregarCarrito"  idProducto="'.$value2["id"].'" imagen="'.$servidor.$value2["portada"].'" titulo="'.$value2["titulo"].'" precio="'.$value2["precio"].'" tipo="'.$value2["tipo"].'" peso="'.$value2["peso"].'" data-toggle="tooltip" title="Agregar al carrito de compras">

											<i class="fa fa-shopping-cart" aria-hidden="true"></i>

											</button>';

										}

									}

						  		echo '<a href="'.$url.$value2["ruta"].'" class="pixelProducto">

							  		<button type="button" class="btn btn-default btn-xs" data-toggle="tooltip" title="Ver producto">

							  		<i class="fa fa-eye" aria-hidden="true"></i>

							  		</button>

						  		</a>
							
							</div>

						</div>

						<div class="col-xs-12"><hr></div>

					</li>';

					}

				}

				echo '</ul>';
			}


			?>










		  	</div>

			<!--=====================================
			PESTAÑA PERFIL
			======================================-->
		  	
		  	<div id="perfil" class="tab-pane fade">
		    	
				<div class="row">
					
					<form method="post" enctype="multipart/form-data">
					
						<div class="col-md-3 col-sm-4 col-xs-12 text-center">
							
							<br>

							<figure id="imgPerfil">
								
							<?php

							echo '<input type="hidden" value="'.$_SESSION["id"].'" id="idUsuario" name="idUsuario">
							      <input type="hidden" value="'.$_SESSION["password"].'" name="passUsuario">
							      <input type="hidden" value="'.$_SESSION["foto"].'" name="fotoUsuario" id="fotoUsuario">
							      <input type="hidden" value="'.$_SESSION["modo"].'" name="modoUsuario" id="modoUsuario">';


							if($_SESSION["modo"] == "directo"){

								if($_SESSION["foto"] != ""){

									echo '<img src="'.$url.$_SESSION["foto"].'" class="img-thumbnail">';

								}else{

									echo '<img src="'.$servidor.'vistas/img/usuarios/default/anonymous.png" class="img-thumbnail">';

								}
					

							}else{

								echo '<img src="'.$_SESSION["foto"].'" class="img-thumbnail">';
							}		

							?>

							</figure>

							<br>

							<?php

							if($_SESSION["modo"] == "directo"){
							
							echo '<button type="button" class="btn btn-default" id="btnCambiarFoto">
									
									Cambiar foto de perfil
									
									</button>';

							}

							?>

							<div id="subirImagen">
								
								<input type="file" class="form-control" id="datosImagen" name="datosImagen">

								<img class="previsualizar">

							</div>

						</div>	

						<div class="col-md-9 col-sm-8 col-xs-12">

						<br>
							
						<?php

						if($_SESSION["modo"] != "directo"){

							echo '<label class="control-label text-muted text-uppercase">Nombre:</label>
									
									<div class="input-group">
								
										<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
										<input type="text" class="form-control"  value="'.$_SESSION["nombre"].'" readonly>

									</div>

									<br>

									<label class="control-label text-muted text-uppercase">Correo electrónico:</label>
									
									<div class="input-group">
								
										<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
										<input type="text" class="form-control"  value="'.$_SESSION["email"].'" readonly>

									</div>

									<br>

									<label class="control-label text-muted text-uppercase">Modo de registro en el sistema:</label>
									
									<div class="input-group">
								
										<span class="input-group-addon"><i class="fa fa-'.$_SESSION["modo"].'"></i></span>
										<input type="text" class="form-control text-uppercase"  value="'.$_SESSION["modo"].'" readonly>

									</div>

									<br>';
		

						}else{

							echo '<label class="control-label text-muted text-uppercase" for="editarNombre">Cambiar Nombre:</label>
									
									<div class="input-group">
								
										<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
										<input type="text" class="form-control" id="editarNombre" name="editarNombre" value="'.$_SESSION["nombre"].'">

									</div>

								<br>

								<label class="control-label text-muted text-uppercase" for="editarEmail">Cambiar Correo Electrónico:</label>

								<div class="input-group">
								
										<span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
										<input type="text" class="form-control" id="editarEmail" name="editarEmail" value="'.$_SESSION["email"].'">

									</div>

								<br>

								<label class="control-label text-muted text-uppercase" for="editarPassword">Cambiar Contraseña:</label>

								<div class="input-group">
								
										<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
										<input type="text" class="form-control" id="editarPassword" name="editarPassword" placeholder="Escribe la nueva contraseña">

									</div>

								<br>

								<button type="submit" class="btn btn-default backColor btn-md pull-left">Actualizar Datos</button>';

						}

						?>

						</div>

						<?php

							$actualizarPerfil = new ControladorUsuarios();
							$actualizarPerfil->ctrActualizarPerfil();

						?>					

					</form>

					<button class="btn btn-danger btn-md pull-right" id="eliminarUsuario">Eliminar cuenta</button>

					<?php

							$borrarUsuario = new ControladorUsuarios();
							$borrarUsuario->ctrEliminarUsuario();

						?>	

				</div>

		  	</div>

		</div>

	</div>

</div>

<!--=====================================
VENTANA MODAL PARA COMENTARIOS
======================================-->

<div  class="modal fade modalFormulario" id="modalComentarios" role="dialog">
	
	<div class="modal-content modal-dialog">
		
		<div class="modal-body modalTitulo">
			
			<h3 class="backColor">CALIFICA ESTE PRODUCTO</h3>

			<button type="button" class="close" data-dismiss="modal">&times;</button>

			<form method="post" onsubmit="return validarComentario()">

				<input type="hidden" value="" id="idComentario" name="idComentario">
				
				<h1 class="text-center" id="estrellas">

		       		<i class="fa fa-star text-success" aria-hidden="true"></i>
					<i class="fa fa-star text-success" aria-hidden="true"></i>
					<i class="fa fa-star text-success" aria-hidden="true"></i>
					<i class="fa fa-star text-success" aria-hidden="true"></i>
					<i class="fa fa-star text-success" aria-hidden="true"></i>

				</h1>

				<div class="form-group text-center">

		       		<label class="radio-inline"><input type="radio" name="puntaje" value="0.5">0.5</label>
					<label class="radio-inline"><input type="radio" name="puntaje" value="1.0">1.0</label>
					<label class="radio-inline"><input type="radio" name="puntaje" value="1.5">1.5</label>
					<label class="radio-inline"><input type="radio" name="puntaje" value="2.0">2.0</label>
					<label class="radio-inline"><input type="radio" name="puntaje" value="2.5">2.5</label>
					<label class="radio-inline"><input type="radio" name="puntaje" value="3.0">3.0</label>
					<label class="radio-inline"><input type="radio" name="puntaje" value="3.5">3.5</label>
					<label class="radio-inline"><input type="radio" name="puntaje" value="4.0">4.0</label>
					<label class="radio-inline"><input type="radio" name="puntaje" value="4.5">4.5</label>
					<label class="radio-inline"><input type="radio" name="puntaje" value="5.0" checked>5.0</label>

				</div>

				<div class="form-group">
			  		
			  		<label for="comment" class="text-muted">Dejanos tus datos para que nos podamos comunicar contigo: <span><small>(máximo 300 caracteres)</small></span></label>
			  		
			  		<textarea class="form-control" rows="5" id="comentario" name="comentario" maxlength="300" required></textarea>

			  		<br>
					
					<input type="submit" class="btn btn-default backColor btn-block" value="ENVIAR">

				</div>

				<?php

					$actualizarComentario = new ControladorUsuarios();
					$actualizarComentario -> ctrActualizarComentario();

				?>

			</form>

		</div>

		<div class="modal-footer">
      	
      	</div>

	</div>

</div>