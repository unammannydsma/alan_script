
// Data gathered from http://populationpyramid.net/germany/2015/

// Age categories

$( document ).ready(function() {
    $.ajax({
        url:'modelos/test1.php',
        type:'POST',
        data:{
            "dato1": "dato prueba"
        },
        success: function (respuesta_test1) {
            // setInterval(function(){ 
            //     alert(typeof(respuesta_test1)); 
            // }, 3000);
            console.log("correcto: ")
            respuesta_JSON_prueba = JSON.parse(respuesta_test1)
            console.log(typeof(respuesta_string)) 
            console.log(respuesta_JSON_prueba)
            
        },
        error: function (respuesta_test1) {
            alert("error: "+respuesta_test1);
        }
    })
});

var categories = [
    'Estatura', 'Fortaleza', 'Profundidad', 'Forma Lechera',
    'Angulo Anca', 'Ancho de Grupa', 'Patas Lateral', 'Patas Posterior', 'Angulo Pezuña',
    'Score patas', 'Ubre anterior', 'Altura Ubre Posterior', 'Soporte Central', 'Prof Ubre',
    'Col.pez.ant', 'col.pez.post', 'Largo Pez.'
];

Highcharts.chart('container', {
    chart: {
        type: 'bar'
    },
    title: {
        text: 'AG link Genetics México, 2019'
    },
    subtitle: {
      
    },
    xAxis: [{
        categories: categories,
        reversed: false,
        labels: {
            step: 1
        }
    }, { // mirror axis on right side
        opposite: true,
        reversed: false,
        categories: categories,
        linkedTo: 0,
        labels: {
            step: 1
        }
    }],
    yAxis: {
        title: {
            text: null
        },
        labels: {
            formatter: function () {
                return Math.abs(this.value) + '%';
            }
        }
    },

    plotOptions: {
        series: {
            stacking: 'normal'
        }
    },

    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + ', rasgo ' + this.point.category + '</b><br/>' +
                'rango: ' + Highcharts.numberFormat(Math.abs(this.point.y), 0);
        }
    },

    series: [{
        name: '- Inferior',
        data: [
            -0.0, -0.0, -0.2,  -0.0,
            -0.07, -3.0, -3.3, -3.2,
            -2.9, -3.5, -4.4, -4.1,
            -3.4, -2.7, -2.3, -2.2,
            -1.6
        ]
    }, {
        name: '+ Superior',
        data: [
            0.90, 0.39, 0.0, 2.3, 0.0,
            4.0, 3.2, 3.1, 2.9, 3.4,
            4.3, 4.0, 3.5, 2.9, 2.5,
            2.7, 2.2
        ]
    }]
});

 