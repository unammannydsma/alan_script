<!--=====================================
BARRA PRODUCTOS
======================================-->

<div class="container-fluid well well-sm barraProductos">

	<div class="container">
		
		<div class="row">

			<div class="col-sm-6 col-xs-12">
				
				<div class="btn-group">
					
					 <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">

					  Ordenar Productos <span class="caret"></span></button>

					  <ul class="dropdown-menu" role="menu">

					  <?php
					  	
						echo '<li><a href="'.$url.$rutas[0].'/1/recientes/'.$rutas[3].'">Más reciente</a></li>
							  <li><a href="'.$url.$rutas[0].'/1/antiguos/'.$rutas[3].'">Más antiguo</a></li>';

						?>

					  </ul>

				</div>

			</div>
			
			<div class="col-sm-6 col-xs-12 organizarProductos">

				<div class="btn-group pull-right">

					 <button type="button" class="btn btn-default btnGrid" id="btnGrid0">
					 	
						<i class="fa fa-th" aria-hidden="true"></i>  

						<span class="col-xs-0 pull-right"> GRID</span>

					 </button>

					 <button type="button" class="btn btn-default btnList" id="btnList0">
					 	
						<i class="fa fa-list" aria-hidden="true"></i> 

						<span class="col-xs-0 pull-right"> LIST</span>

					 </button>
					
				</div>		

			</div>

		</div>

	</div>

</div>

<!--=====================================
LISTAR PRODUCTOS
======================================-->

<div class="container-fluid productos">

	<div class="container">
		
		<div class="row">

			<!--=====================================
			BREADCRUMB O MIGAS DE PAN
			======================================-->

			<ul class="breadcrumb fondoBreadcrumb text-uppercase">
				
				<li><a href="<?php echo $url;  ?>">INICIO</a></li>
				<li class="active pagActiva"><?php echo $rutas[0] ?></li>

			</ul>

			<?php

			/*=============================================
			LLAMADO DE PAGINACIÓN
			=============================================*/

			if(isset($rutas[1])){

				if(isset($rutas[2])){

					if($rutas[2] == "antiguos"){

						$modo = "ASC";
						$_SESSION["ordenar"] = "ASC";

					}else{

						$modo = "DESC";
						$_SESSION["ordenar"] = "DESC";

					}

				}else{

					$modo = $_SESSION["ordenar"];

				}

				$base = ($rutas[1] - 1)*12;
				$tope = 12;

			}else{

				$rutas[1] = 1;
				$base = 0;
				$tope = 12;
				$modo = "DESC";
				$_SESSION["ordenar"] = "DESC";

			}

			/*=============================================
			LLAMADO DE PRODUCTOS POR BÚSQUEDA
			=============================================*/

			$productos = null;
			$listaProductos = null;

			$ordenar = "id";

			if(isset($rutas[3])){

				$busqueda = $rutas[3];

				$productos = ControladorProductos::ctrBuscarProductos($busqueda, $ordenar, $modo, $base, $tope);
				$listaProductos = ControladorProductos::ctrListarProductosBusqueda($busqueda);

			}

			if(!$productos){

				$estado = 0;

				echo '<div class="col-xs-12 error404 text-center">

						 <h1><small>¡Oops!</small></h1>

						 <h2>Aún no hay productos en esta sección</h2>

					</div>';

			}else{

				echo '<ul class="grid0">';

					foreach ($productos as $key => $value) {

					if($value["estado"] != 0){

						$estado = 1;
					
					echo '<li class="col-md-3 col-sm-6 col-xs-12">

							<figure>
								
								<a href="'.$url.$value["ruta"].'" class="pixelProducto">
									
									<img src="'.$servidor.$value["portada"].'" class="img-responsive">

								</a>

							</figure>

							'.$value["id"].'

							<h4>
					
								<small>
									
									<a href="'.$url.$value["ruta"].'" class="pixelProducto">
										
										'.$value["titulo"].'<br>

										<span style="color:rgba(0,0,0,0)">-</span>';

										$fecha = date('Y-m-d');
										$fechaActual = strtotime('-30 day', strtotime($fecha));
										$fechaNueva = date('Y-m-d', $fechaActual);

										if($fechaNueva < $value["fecha"]){

											echo '<span class="label label-warning fontSize">Nuevo</span> ';

										}

										if($value["oferta"] != 0 && $value["precio"] != 0){

											echo '<span class="label label-warning fontSize">'.$value["descuentoOferta"].'% off</span>';

										}

									echo '</a>	

								</small>			

							</h4>

							<div class="col-xs-6 precio">';

							if($value["precio"] == 0){

	

							}else{

								if($value["oferta"] != 0){

									echo '<h2>

											<small>
						
												<strong class="oferta">USD $'.$value["precio"].'</strong>

											</small>

											<small>$'.$value["precioOferta"].'</small>
										
										</h2>';

								}else{

									echo '<h2><small>USD $'.$value["precio"].'</small></h2>';

								}
								
							}
											
							echo '</div>

							<div class="col-xs-6 enlaces">
								
								<div class="btn-group pull-right">
									
									<button type="button" class="btn btn-default btn-xs deseos" idProducto="'.$value["id"].'" data-toggle="tooltip" title="Agregar a mi lista de deseos">
										
										<i class="fa fa-heart" aria-hidden="true"></i>

									</button>';

									if($value["tipo"] == "virtual" && $value["precio"] != 0){

										if($value["oferta"] != 0){

											echo '<button type="button" class="btn btn-default btn-xs agregarCarrito"  idProducto="'.$value["id"].'" imagen="'.$servidor.$value["portada"].'" titulo="'.$value["titulo"].'" precio="'.$value["precioOferta"].'" tipo="'.$value["tipo"].'" peso="'.$value["peso"].'" data-toggle="tooltip" title="Agregar al carrito de compras">

											<i class="fa fa-shopping-cart" aria-hidden="true"></i>

											</button>';

										}else{

											echo '<button type="button" class="btn btn-default btn-xs agregarCarrito"  idProducto="'.$value["id"].'" imagen="'.$servidor.$value["portada"].'" titulo="'.$value["titulo"].'" precio="'.$value["precio"].'" tipo="'.$value["tipo"].'" peso="'.$value["peso"].'" data-toggle="tooltip" title="Agregar al carrito de compras">

											<i class="fa fa-shopping-cart" aria-hidden="true"></i>

											</button>';

										}

									}

									echo '<a href="'.$url.$value["ruta"].'" class="pixelProducto">
									
										<button type="button" class="btn btn-default btn-xs" data-toggle="tooltip" title="Ver producto">
											
											<i class="fa fa-eye" aria-hidden="true"></i>

										</button>	
									
									</a>

								</div>

							</div>

						</li>';

					}else{

						$estado = 0;
						
					}
				}

				if($estado == 0){

					echo '<div class="col-xs-12 error404 text-center">

						 <h1><small>¡Oops!</small></h1>

						 <h2>Aún no hay productos en esta sección</h2>

						</div>';
				}

				echo '</ul>

				<ul class="list0" style="display:none">';

				foreach ($productos as $key => $value) {

					if($value["estado"] != 0){

						echo '<li class="col-xs-12">
						  
					  		<div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
								   
								<figure>
							
									<a href="'.$url.$value["ruta"].'" class="pixelProducto">
										
										<img src="'.$servidor.$value["portada"].'" class="img-responsive">

									</a>

								</figure>

						  	</div>  	
								  
							<div class="col-lg-10 col-md-7 col-sm-8 col-xs-12">
								
								<h1>

									<small>

										<a href="'.$url.$value["ruta"].'" class="pixelProducto">
											
											'.$value["titulo"].'<br>';

											$fecha = date('Y-m-d');
											$fechaActual = strtotime('-30 day', strtotime($fecha));
											$fechaNueva = date('Y-m-d', $fechaActual);

											if($fechaNueva < $value["fecha"]){

												echo '<span class="label label-warning">Nuevo</span> ';

											}

											if($value["oferta"] != 0 && $value["precio"] != 0){

												echo '<span class="label label-warning">'.$value["descuentoOferta"].'% off</span>';

											}		

										echo '</a>

									</small>

								</h1>

								<p class="text-muted">'.$value["titular"].'</p>';







//catalogo caracteristicas listas

                            	

				if($value["tipo"] == "fisico"){

					echo '<div class="col-md-12 col-sm-12 col-xs-12">';

                     echo'                           		
<style>
					<style>
table { 
  width: 100%; 
  border-collapse: collapse; 
  text-align:center;
}
/* Zebra striping */
tr:nth-of-type(odd) { 
  background: #ffcccc; 
}
th { 
  background: #364f93; 
  color: white; 
  font-weight: bold; 
}
td, th { 
  padding: 6px; 
  border: 1px solid #ccc; 
  text-align: center; 
  font-weight: bold; 
}

</style>
</head>
<body>


<div style="overflow-x:auto;">
  <table>
    <tr>
      <td></td>

         <th>NAAB CODE</th>
      <th>REG NAME</th>
      <th>CED</th>
      <th>BW</th>
      <th>WW</th>
      <th>YW</th>
      <th>DMI</th>

      <th>MILK</th>
      <th>TM</th>
      <th>CEM</th>
      <th>SC</th>
      <th>REA</th>
      <th>IMF</th>
      <th>FAT</th>
  
      <th>CW</th>
      <th>YH</th>
      <th>HP</th>
      <th>MCW</th>
      <th>MARB</th>
      <th>BMI</th>
      <th>BII</th>
      <th>CHB</th>
      <th>SIRE NAME</th>

    </tr>
    <tr>
       <th></th>
      <td >'.$value["naab code fisicoproducto"].'</td>
      <td>'.$value["regname"].'</td>
      <td>'.$value["ced"].'</td>
      <td>'.$value["bw"].'</td>
      <td>'.$value["ww"].'</td>
      <td>'.$value["yw"].'</td>
      <td>'.$value["dmi"].'</td>
      <td>'.$value["milk"].'</td>
      <td>'.$value["tm"].'</td>
      <td>'.$value["cem"].'</td>
      <td>'.$value["sc"].'</td>
      <td>'.$value["rea"].'</td>
      <td>'.$value["imf"].'</td>
      <td>'.$value["fatt"].'</td>
      <td>'.$value["cwfisico"].'</td>
      <td>'.$value["yh"].'</td>
      <td>'.$value["hpfisico"].'</td>
       <td>'.$value["mcw"].'</td>
        <td>'.$value["marb"].'</td>
         <td>'.$value["bmi"].'</td>
          <td>'.$value["bii"].'</td>
           <td>'.$value["chb"].'</td>
            <td>'.$value["sirename fisico"].'</td>
        

    </tr>
 
  
  </table>
</div>

<br><br>

<hr>';






				}else if($value["tipo"] == "virtual"){

	




					echo '<div class="col-xs-12">
					<style>
table { 
  width: 100%; 
  border-collapse: collapse; 
  text-align:center;
}
/* Zebra striping */
tr:nth-of-type(odd) { 
  background: #ffcccc; 
}
th { 
  background: #364f93; 
  color: white; 
  font-weight: bold; 
}
td, th { 
  padding: 6px; 
  border: 1px solid #ccc; 
  text-align: center; 
  font-weight: bold; 
}

</style>
</head>
<body>


<div style="overflow-x:auto;">
  <table>


	<div class="col-lg-10 col-md-7 col-sm-8 col-xs-12">

	<small>
    <table id="table_id" class="table table-bordered table-striped table-hover">
        <thead>
            <tr>
                <th>Naab Code</th>
                <th>Name</th>
                <th>Reg Name</th>
                <th>A2A2t</th>
                <th>Birth Date</th>
                <th>Obs</th>
                <th>RHA</th>
                <th>Eco Feed</th>
                <th>TPI</th>
                <th>JPI</th>
                <th>PPR</th>
                <th>Net Merit</th>
                <th>$CM</th>
                <th>Grazing Merit</th>
                <th>PTA Milk</th>
                <th>PTA fat</th>
                <th>%fat</th>
                <th>PTA Pro</th>
                <th> % Pro</th>
                <th>CFP</th>
                <th> Milk Rel</th>
                <th>SCS</th>
                <th>PL</th>
                <th>PTA DPR</th>
                <th>PTA LIV</th>
                <th>Fertil Index</th>
                <th>PTA Type</th>
                <th>UDC</th>
                <th>JUI</th>
                <th>MO</th>
                <th>FLC</th>
                <th>SCE</th>
                <th>DLE</th>
                <th>SSB</th>
                <th>DSB</th>
                <th>SIRE</th>
                <th>MGS</th>
                <th>MGGS</th>
                <th>AAa</th>
                <th>DMS</th>


            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="text-center">'.$value["nabcode"].'</td>
                <td>'.$value["name"].'</td>
                <td>'.$value["regnamee"].'</td>
                <td class="text-right">'.$value["a2a2t"].'</td>
                <td class="text-center">'.$value["birthdate"].'</td>
                <td class="text-center">'.$value["obs"].'</td>
                <td class="text-center">'.$value["RHA"].'</td>
                <td class="text-center">'.$value["ecofeed"].'</td>
                <td class="text-center">'.$value["tpii"].'</td>
                <td class="text-center">'.$value["jpi"].'</td>
                <td class="text-center">'.$value["ppr"].'</td>
                <td class="text-center">'.$value["netmerito"].'</td>
                <td class="text-center">'.$value["productocm"].'</td>
                <td class="text-center">'.$value["grazingmerito"].'</td>
                <td class="text-center">'.$value["ptamilk"].'</td>
                <td class="text-center">'.$value["ptafat"].'</td>
                <td class="text-center">'.$value["porcentfat"].'</td>
                <td class="text-center">'.$value["ptapro"].'</td>
                <td class="text-center">'.$value["porcentpro"].'</td>
                <td class="text-center">'.$value["cfp"].'</td>
                <td class="text-center">'.$value["milkrel"].'</td>
                <td class="text-center">'.$value["productoscs"].'</td>
                <td class="text-center">'.$value["productopl"].'</td>
                <td class="text-center">'.$value["ptadpr"].'</td>
                <td class="text-center">'.$value["ptaliv"].'</td>
                <td class="text-center">'.$value["fertilindex"].'</td>
                <td class="text-center">'.$value["ptatype"].'</td>
                <td class="text-center">'.$value["udc"].'</td>
                <td class="text-center">'.$value["jui"].'</td>
                <td class="text-center">'.$value["moo"].'</td>
                <td class="text-center">'.$value["flc"].'</td>
                <td class="text-center">'.$value["sce"].'</td>
                <td class="text-center">'.$value["dle"].'</td>
                <td class="text-center">'.$value["ssb"].'</td>
                <td class="text-center">'.$value["dsb"].'</td>
                <td class="text-center">'.$value["sire"].'</td>
                <td class="text-center">'.$value["mgs"].'</td>
                <td class="text-center">'.$value["mggs"].'</td>
                <td class="text-center">'.$value["aAa"].'</td>
                <td class="text-center">'.$value["dms"].'</td>

            </tr>

        </tbody>
        <tfood>
            <tr>
               
            </tr>
        </tfood>
    </table>


<br><br>








';



	}if($value["tipo"] == "producto"){


echo'










';




	}

		



								

								if($value["precio"] == 0){

									echo '</h2>';

								}else{

									if($value["oferta"] != 0){

										echo '<h2>

												<small>
							
													<strong class="oferta">USD $'.$value["precio"].'</strong>

												</small>

												<small>$'.$value["precioOferta"].'</small>
											
											</h2>';

									}else{

										echo '<h2><small>USD $'.$value["precio"].'</small></h2>';

									}
									
								}

								echo '<div class="btn-group pull-left enlaces">
							  	
							  		<button type="button" class="btn btn-default btn-xs deseos"  idProducto="'.$value["id"].'" data-toggle="tooltip" title="Agregar a mi lista de deseos">

							  			<i class="fa fa-heart" aria-hidden="true"></i>

							  		</button>';

							  		if($value["tipo"] == "virtual" && $value["precio"] != 0){

											if($value["oferta"] != 0){

												echo '<button type="button" class="btn btn-default btn-xs agregarCarrito"  idProducto="'.$value["id"].'" imagen="'.$servidor.$value["portada"].'" titulo="'.$value["titulo"].'" precio="'.$value["precioOferta"].'" tipo="'.$value["tipo"].'" peso="'.$value["peso"].'" data-toggle="tooltip" title="Agregar al carrito de compras">

												<i class="fa fa-shopping-cart" aria-hidden="true"></i>

												</button>';

											}else{

												echo '<button type="button" class="btn btn-default btn-xs agregarCarrito"  idProducto="'.$value["id"].'" imagen="'.$servidor.$value["portada"].'" titulo="'.$value["titulo"].'" precio="'.$value["precio"].'" tipo="'.$value["tipo"].'" peso="'.$value["peso"].'" data-toggle="tooltip" title="Agregar al carrito de compras">

												<i class="fa fa-shopping-cart" aria-hidden="true"></i>

												</button>';

											}

										}

							  		echo '<a href="'.$url.$value["ruta"].'" class="pixelProducto">

								  		<button type="button" class="btn btn-default btn-xs" data-toggle="tooltip" title="Ver producto">

								  		<i class="fa fa-eye" aria-hidden="true"></i>

								  		</button>

							  		</a>
								
								</div>

							</div>

							<div class="col-xs-12"><hr></div>

						</li>';

					}else{

						$estado = 0;	
					}

				}

				if($estado == 0){

					echo '<div class="col-xs-12 error404 text-center">

						 <h1><small>¡Oops!</small></h1>

						 <h2>Aún no hay productos en esta sección</h2>

						</div>';
				}


				echo '</ul>';
			}

			?>

			<div class="clearfix"></div>

			<center>

			<!--=====================================
			PAGINACIÓN
			======================================-->
			
			<?php

			if($estado != 0){

				if(count($listaProductos) != 0){

					$pagProductos = ceil(count($listaProductos)/12);

					if($pagProductos > 4){

						/*=============================================
						LOS BOTONES DE LAS PRIMERAS 4 PÁGINAS Y LA ÚLTIMA PÁG
						=============================================*/

						if($rutas[1] == 1){

							echo '<ul class="pagination">';
							
							for($i = 1; $i <= 4; $i ++){

								echo '<li id="item'.$i.'"><a href="'.$url.$rutas[0].'/'.$i.'/'.$rutas[2].'/'.$rutas[3].'">'.$i.'</a></li>';

							}

							echo ' <li class="disabled"><a>...</a></li>
								   <li id="item'.$pagProductos.'"><a href="'.$url.$rutas[0].'/'.$pagProductos.'/'.$rutas[2].'/'.$rutas[3].'">'.$pagProductos.'</a></li>
								   <li><a href="'.$url.$rutas[0].'/2/'.$rutas[2].'/'.$rutas[3].'"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>

							</ul>';

						}

						/*=============================================
						LOS BOTONES DE LA MITAD DE PÁGINAS HACIA ABAJO
						=============================================*/

						else if($rutas[1] != $pagProductos && 
							    $rutas[1] != 1 &&
							    $rutas[1] <  ($pagProductos/2) &&
							    $rutas[1] < ($pagProductos-3)
							    ){

								$numPagActual = $rutas[1];

								echo '<ul class="pagination">
									  <li><a href="'.$url.$rutas[0].'/'.($numPagActual-1).'/'.$rutas[2].'/'.$rutas[3].'"><i class="fa fa-angle-left" aria-hidden="true"></i></a></li> ';
							
								for($i = $numPagActual; $i <= ($numPagActual+3); $i ++){

									echo '<li id="item'.$i.'"><a href="'.$url.$rutas[0].'/'.$i.'/'.$rutas[2].'/'.$rutas[3].'">'.$i.'</a></li>';

								}

								echo ' <li class="disabled"><a>...</a></li>
									   <li id="item'.$pagProductos.'"><a href="'.$url.$rutas[0].'/'.$pagProductos.'/'.$rutas[2].'/'.$rutas[3].'">'.$pagProductos.'</a></li>
									   <li><a href="'.$url.$rutas[0].'/'.($numPagActual+1).'/'.$rutas[2].'/'.$rutas[3].'"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>

								</ul>';

						}

						/*=============================================
						LOS BOTONES DE LA MITAD DE PÁGINAS HACIA ARRIBA
						=============================================*/

						else if($rutas[1] != $pagProductos && 
							    $rutas[1] != 1 &&
							    $rutas[1] >=  ($pagProductos/2) &&
							    $rutas[1] < ($pagProductos-3)
							    ){

								$numPagActual = $rutas[1];
							
								echo '<ul class="pagination">
								   <li><a href="'.$url.$rutas[0].'/'.($numPagActual-1).'/'.$rutas[2].'/'.$rutas[3].'"><i class="fa fa-angle-left" aria-hidden="true"></i></a></li> 
								   <li id="item1"><a href="'.$url.$rutas[0].'/1/'.$rutas[2].'/'.$rutas[3].'">1</a></li>
								   <li class="disabled"><a>...</a></li>
								';
							
								for($i = $numPagActual; $i <= ($numPagActual+3); $i ++){

									echo '<li id="item'.$i.'"><a href="'.$url.$rutas[0].'/'.$i.'/'.$rutas[2].'/'.$rutas[3].'">'.$i.'</a></li>';

								}


								echo '  <li><a href="'.$url.$rutas[0].'/'.($numPagActual+1).'/'.$rutas[2].'/'.$rutas[3].'"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
									</ul>';
						}

						/*=============================================
						LOS BOTONES DE LAS ÚLTIMAS 4 PÁGINAS Y LA PRIMERA PÁG
						=============================================*/

						else{

							$numPagActual = $rutas[1];

							echo '<ul class="pagination">
								   <li><a href="'.$url.$rutas[0].'/'.($numPagActual-1).'/'.$rutas[2].'/'.$rutas[3].'"><i class="fa fa-angle-left" aria-hidden="true"></i></a></li> 
								   <li id="item1"><a href="'.$url.$rutas[0].'/1/'.$rutas[2].'/'.$rutas[3].'">1</a></li>
								   <li class="disabled"><a>...</a></li>
								';
							
							for($i = ($pagProductos-3); $i <= $pagProductos; $i ++){

								echo '<li id="item'.$i.'"><a href="'.$url.$rutas[0].'/'.$i.'/'.$rutas[2].'/'.$rutas[3].'">'.$i.'</a></li>';

							}

							echo ' </ul>';

						}

					}else{

						echo '<ul class="pagination">';
						
						for($i = 1; $i <= $pagProductos; $i ++){

							echo '<li id="item'.$i.'"><a href="'.$url.$rutas[0].'/'.$i.'/'.$rutas[2].'/'.$rutas[3].'">'.$i.'</a></li>';

						}

						echo '</ul>';

					}

				}

			}

			?>

			</center>

</div>

</div>

</div>