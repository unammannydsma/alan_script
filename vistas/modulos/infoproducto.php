<?php

$servidor = Ruta::ctrRutaServidor();
$url = Ruta::ctrRuta();

?>

<!--=====================================
BREADCRUMB INFOPRODUCTOS
======================================-->
<div class="container-fluid well well-sm">
	
	<div class="container">
		
		<div class="row">
			
			<ul class="breadcrumb fondoBreadcrumb text-uppercase">
				
				<li><a href="<?php echo $url;  ?>">INICIO</a></li>
				<li class="active pagActiva"><?php echo $rutas[0] ?></li>

			</ul>

		</div>

	</div>

</div>

<!--=====================================
INFOPRODUCTOS
======================================-->
<div class="container-fluid infoproducto">
	
	<div class="container">
		
		<div class="row">

			<?php

				$item =  "ruta";
				$valor = $rutas[0];
				$infoproducto = ControladorProductos::ctrMostrarInfoProducto($item, $valor);

				$multimedia = json_decode($infoproducto["multimedia"],true);


				/*=============================================
				VISOR DE IMÁGENES
				=============================================*/

				if($infoproducto["tipo"] == "fisico"){

					echo '<div class="col-md-5 col-sm-6 col-xs-12 visorImg">
						
							<figure class="visor">';

							if($multimedia != null){

								for($i = 0; $i < count($multimedia); $i ++){

									echo '<img id="lupa'.($i+1).'" class="img-thumbnail" src="'.$servidor.$multimedia[$i]["foto"].'">';

								}								

								echo '</figure>

								<div class="flexslider">
								  
								  <ul class="slides">';

								for($i = 0; $i < count($multimedia); $i ++){

									echo '<li>
								     	<img value="'.($i+1).'" class="img-thumbnail" src="'.$servidor.$multimedia[$i]["foto"].'" alt="'.$infoproducto["titulo"].'">






								    </li>';

								}

							}		
							    						 
							  echo '</ul>

							</div>

						</div>';			

				}else{

					/*=============================================
					VISOR DE VIDEO
					=============================================*/

					echo '<div class="col-sm-6 col-xs-12">
							    
					<img src="'.$servidor.$infoproducto["portada"].'" class="img-responsive">

					</div>

                    <br><br>


					';



				}			

			?>
                 <br><br>
			<!--=====================================
			PRODUCTO aqui
			======================================-->

			<?php

				if($infoproducto["tipo"] == "fisico"){

					echo '<div class="col-md-7 col-sm-6 col-xs-12">';




				}else{

					echo '<div class="col-sm-6 col-xs-12">';
				}

			?>

				<!--=====================================
				REGRESAR A LA TIENDA
				======================================-->

				<div class="col-xs-6">
					
					<h6>
						
						<a href="javascript:history.back()" class="text-muted">
							
							<i class="fa fa-reply"></i> Continuar Busqueda

						</a>

					</h6>

				</div>

				<!--=====================================
				COMPARTIR EN REDES SOCIALES
				======================================-->

				<div class="col-xs-6">
					
					<h6>
						
						<a class="dropdown-toggle pull-right text-muted" data-toggle="dropdown" href="">
							
							<i class="fa fa-plus"></i> Compartir

						</a>

						<ul class="dropdown-menu pull-right compartirRedes">

							<li>
								<p class="btnFacebook">
									<i class="fa fa-facebook"></i>
									Facebook
								</p>
							</li>

							<li>
								<p class="btnGoogle">
									<i class="fa fa-google"></i>
									Google
								</p>
							</li>
							
						</ul>

					</h6>

				</div>

				<div class="clearfix"></div>

				<!--=====================================
				ESPACIO PARA EL PRODUCTO
				======================================-->

				<?php

					echo '<div class="comprarAhora" style="display:none">


						<button class="btn btn-default backColor quitarItemCarrito" idProducto="'.$infoproducto["id"].'" peso="'.$infoproducto["peso"].'"></button>

						<p class="tituloCarritoCompra text-left">'.$infoproducto["titulo"].'</p>';


						if($infoproducto["oferta"] == 0){

							echo'<input class="cantidadItem" value="1" tipo="'.$infoproducto["tipo"].'" precio="'.$infoproducto["precio"].'" idProducto="'.$infoproducto["id"].'">

							<p class="subTotal'.$infoproducto["id"].' subtotales">
						
								<strong>USD $<span>'.$infoproducto["precio"].'</span></strong>

							</p>

							<div class="sumaSubTotal"><span>'.$infoproducto["precio"].'</span></div>';


						}else{

							echo'<input class="cantidadItem" value="1" tipo="'.$infoproducto["tipo"].'" precio="'.$infoproducto["precioOferta"].'" idProducto="'.$infoproducto["id"].'">

							<p class="subTotal'.$infoproducto["id"].' subtotales">
						
								<strong>USD $<span>'.$infoproducto["precioOferta"].'</span></strong>

							</p>

							<div class="sumaSubTotal"><span>'.$infoproducto["precioOferta"].'</span></div>';


						}

					




					echo '</div>';







					/*=============================================
					TITULO
					=============================================*/				
					
					if($infoproducto["oferta"] == 0){

						$fecha = date('Y-m-d');
						$fechaActual = strtotime('-30 day', strtotime($fecha));
						$fechaNueva = date('Y-m-d', $fechaActual);

						if($fechaNueva > $infoproducto["fecha"]){

							echo '<h1 class="text-muted text-uppercase">'.$infoproducto["titulo"].'</h1>';

						}else{

							echo '<h1 class="text-muted text-uppercase">'.$infoproducto["titulo"].'

							<br>

							<small>
						
								<span class="label label-warning">Nuevo</span>

							</small>

							</h1>';

						}

					}else{

						$fecha = date('Y-m-d');
						$fechaActual = strtotime('-30 day', strtotime($fecha));
						$fechaNueva = date('Y-m-d', $fechaActual);

						if($fechaNueva > $infoproducto["fecha"]){

							echo '<h1 class="text-muted text-uppercase">'.$infoproducto["titulo"].'

							<br>';

							if($infoproducto["precio"] != 0){

								echo '<small>
							
									<span class="label label-warning">'.$infoproducto["descuentoOferta"].'% off</span>

								</small>';

							}
							
							echo '</h1>';

						}else{

							echo '<h1 class="text-muted text-uppercase">'.$infoproducto["titulo"].'

							<br>';

							if($infoproducto["precio"] != 0){

								echo '<small>
									<span class="label label-warning">Nuevo</span> 
									<span class="label label-warning">'.$infoproducto["descuentoOferta"].'% off</span> 

								</small>';

							}
							
							echo '</h1>';

						}
					}














					/*=============================================
					TITULO
					=============================================*/	

					if($infoproducto["precio"] == 0){

						echo '<h2 class="text-muted"></h2>';

					}else{

						if($infoproducto["oferta"] == 0){

							echo '<h2 class="text-muted">USD $'.$infoproducto["precio"].'</h2>';

						}else{

							echo '<h2 class="text-muted">

								<span>
								
									<strong class="oferta">USD $'.$infoproducto["precio"].'</strong>

								</span>

								<span>
									
									$'.$infoproducto["precioOferta"].'

								</span>

							</h2>';

						}

					}

					/*=============================================
					DESCRIPCIÓN
					=============================================*/		

					echo '<p>'.$infoproducto["descripcion"].'</p>';

           

               

				if($infoproducto["tipo"] == "fisico"){

				
                     echo'                           		



  <tr>
    <th>tatto :  <td>'.$infoproducto["tatto"].'</td></th><br>

    <th>Born :  <td>'.$infoproducto["born"].'</td></th> <br>
    <th>Birth Wt : <td> <td>'.$infoproducto["birth wt"].'</td></td></th><br>
     <th>Weaning Wt : <td> <td>'.$infoproducto["weanig wt"].'</td></td> </th> <br>
        <th>Yearling Wt : <td> <td>'.$infoproducto["yearling wt"].'</td></td> </th> <br>
           <th>Yearling SC : <td> <td>'.$infoproducto["yearling sc"].'</td></td> </th> <br>
              <th>Yearling Frame : <td> <td>'.$infoproducto["yearling frame"].'</td></td> </th> <br>
        

        <th>FROM : <td>'.$infoproducto["fromm"].'</td> </th> <br>
  </tr>
<br><br>
';











				}else if($infoproducto["tipo"] == "virtual"){

	




					echo '




';



	}else{
   echo'




<style>
#customers {
  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#customers td, #customers th {
  border: 1px solid #ddd;
  padding: 8px;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}

#customers th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #4CAF50;
  color: white;
}
</style>
</head>
<body>

    <h4 style = "color:red;">Padre </h4>'.$infoproducto["born"].'</td></th> <br>
  <hr>
  
    <h4 style = "color:red;" >Madre  </h4>'.$infoproducto["born"].'</td></th> <br>


<table id="customers">
  <tr>
    <th>n°</th>
    <th>año</th>
    <th>dias</th>
    <th>leche</th>
    <th>G %</th>
    <th>P</th>

  </tr>
  <tr>
    <td>1</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>2</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>3</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>4</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>5</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
        <td></td>
  </tr>
   




</table>

</body>


<style>
table {
  border-collapse: collapse;
  width: 50%;
}

th, td {
  padding: 8px;
  text-align: left;
  border-bottom: 1px solid #ddd;
}

tr:hover {background-color:#f5f5f5;}
</style>
</head>
<body>

<h2>INDICE PEDIGRI</h2>
<p>Move the mouse over the table rows to see the effect.</p>

<table>
  <tr>
    <th>First Name</th>
    <th>Last Name</th>
    <th>Points</th>
  </tr>
  <tr>
    <td>Peter</td>
    <td>Griffin</td>
    <td>$100</td>
  </tr>
  <tr>
    <td>Lois</td>
    <td>Griffin</td>
    <td>$150</td>
  </tr>
  <tr>
    <td>Joe</td>
    <td>Swanson</td>
    <td>$300</td>
  </tr>
  <tr>
    <td>Cleveland</td>
    <td>Brown</td>
    <td>$250</td>
  </tr>
</table>

</body>


   ';
          




	}

			?>

			







				<!--=====================================
				CARACTERÍSTICAS DEL PRODUCTO
				======================================-->

			
<div class="col-md-10 col-sm-10 col-xs-12">  

              
		


					
				<?php

					if($infoproducto["detalles"] != null){

						$detalles = json_decode($infoproducto["detalles"], true);

						if($infoproducto["tipo"] == "fisico"){

							if($detalles["Talla"]!=null){

								echo '<div class="col-md-3 col-xs-12">

								
										';

										for($i = 0; $i <= count($detalles["Talla"]); $i++){

											echo '';

										}

									echo '</select>

								</div>';

							}

							if($detalles["Color"]!=null){

								echo '<div class="col-md-3 col-xs-12">

									<select class="form-control seleccionarDetalle" id="seleccionarColor">
										
										<option value="">Color</option>';

										for($i = 0; $i <= count($detalles["Color"]); $i++){

											echo '<option value="'.$detalles["Color"][$i].'">'.$detalles["Color"][$i].'</option>';

										}

									echo '</select>

								</div>';

							}

							if($detalles["Marca"]!=null){

								echo '<div class="col-md-3 col-xs-12">

									<select class="form-control seleccionarDetalle" id="seleccionarMarca">
										
										<option value="">Marca</option>';

										for($i = 0; $i <= count($detalles["Marca"]); $i++){

											echo '<option value="'.$detalles["Marca"][$i].'">'.$detalles["Marca"][$i].'</option>';

										}

									echo '</select>

								</div>';

							}

						}else{

							

						}

					}





					/*=============================================
					ENTREGA
					=============================================*/

					if($infoproducto["entrega"] == 0){

						if($infoproducto["precio"] == 0){

							echo '



							';

						}else{

							echo '';

						}

					}else{

						if($infoproducto["precio"] == 0){

							echo '';

						}else{



				














						}

					}				

				




				?>

				</div>

				
				
				<!--=====================================
				ZONA DE LUPA
				======================================-->

				<figure class="lupa">
					
					<img src="">

				</figure>

			</div>
			
		</div>




<!--=====================================
		TIPO	PRODUCTO aqui

	======================================-->

			<?php

				if($infoproducto["tipo"] == "fisico"){

					echo '<div class="col-md-12 col-sm-6 col-xs-12">';

                     echo'                           		
<style>


table { 
  width: 100%; 
  border-collapse: collapse; 
  text-align:center;
}
/* Zebra striping */
tr:nth-of-type(odd) { 
  background: #ffcccc; 
}
th { 
  background: #364f93; 
  color: white; 
  font-weight: bold; 
}
td, th { 
  padding: 6px; 
  border: 1px solid #ccc; 
  text-align: center; 
  font-weight: bold; 
}


tr:nth-child(even){background-color: #f2f2f2}
</style>
</head>
<body>


<div style="overflow-x:auto;">
  <table>
    <tr>
      <td></td>


       <th>CED</th>
       <th>BW</th>
       <th>HB</th>
       <th>WW</th>
       <th>GM</th>
       <th>YW</th>
       <th>DMI</th>
       <th>HPG</th>
       <th>MILK</th>
       <th>TM</th>
       <th>CEM</th>
       <th>SC</th>
       <th>STAY</th>
       <th>REA</th>
         <th>IMF</th>	
               <th>FAT</th>			

   







    </tr>
    <tr>
      <th  >EPD</th>
      <td>'.$infoproducto["infofisicoepdCED"].'</td>
      <td>'.$infoproducto["epdBW"].'</td>
      <td>'.$infoproducto["epdHB"].'</td>
      <td>'.$infoproducto["epdWW"].'</td>
      <td>'.$infoproducto["epdGM"].'</td>
      <td>'.$infoproducto["epdYW"].'</td>
      <td>'.$infoproducto["epdDMI"].'</td>
      <td>'.$infoproducto["epdHPG"].'</td>
      <td>'.$infoproducto["epdMILK"].'</td>
      <td>'.$infoproducto["epdTM"].'</td>
      <td>'.$infoproducto["epdCEM"].'</td>
      <td>'.$infoproducto["epdSC"].'</td>
      <td>'.$infoproducto["epdSTAY"].'</td>
      <td>'.$infoproducto["epdREA"].'</td>
      <td>'.$infoproducto["epdIMF"].'</td>
      <td>'.$infoproducto["epdFAT"].'</td>
    </tr>
    <tr>
      <th>ACC</th>
      <td>'.$infoproducto["accCED"].'</td>
      <td>'.$infoproducto["accBW"].'</td>
      <td>'.$infoproducto["accHB"].'</td>
      <td>'.$infoproducto["accWW"].'</td>
      <td>'.$infoproducto["accGM"].'</td>
      <td>'.$infoproducto["accYW"].'</td>
      <td>'.$infoproducto["accDMI"].'</td>
      <td>'.$infoproducto["accHPG"].'</td>
      <td>'.$infoproducto["accMILK"].'</td>
      <td>'.$infoproducto["accTM"].'</td>
      <td>'.$infoproducto["accCEM"].'</td>
      <td>'.$infoproducto["accSC"].'</td>
      <td>'.$infoproducto["accSTAY"].'</td>
      <td>'.$infoproducto["accREA"].'</td>
      <td>'.$infoproducto["accIMF"].'</td>
      <td>'.$infoproducto["accFAT"].'</td>
    </tr>
  
  </table>
</div>

<br><br>

';






				}else if($infoproducto["tipo"] == "virtual"){


	




					echo '<div class="col-xs-12">

					<style>
table { 
  width: 90%; 
  border-collapse: collapse; 
  text-align:center;
}
/* Zebra striping */
tr:nth-of-type(odd) { 
  background: #ffcccc; 
}
th { 
  background: #364f93; 
  color: white; 
  font-weight: bold; 
}
td, th { 
  padding: 6px; 
  border: 1px solid #ccc; 
  text-align: center; 
  font-weight: bold; 
}








</style>
  <table>
	<thead>
	<tr>
		<th>'.$infoproducto["fechasumary"].'</th>
		<th>CDCB</th>
		<th colspan="2">Summary    -   Genomyc</th>
		<th style= "background-color: #ff3333">'.$infoproducto["NM$"].'</th> 

	
	</tr>
	</thead>
	<tbody>
	<tr>
		<th>Milk</th>
		<td>'.$infoproducto["infomilk"].'</td>
         <td>'.$infoproducto["sumarymilk"].'</td>

	     <th>Fluid Merit $</th>
            <td>'.$infoproducto["fluidmerit$"].'</td>
     

	</tr>
	<tr>
		<th>Fat</th>
		<td>'.$infoproducto["fat"].'</td>

		<td>'.$infoproducto["sumaryfat"].'</td>
	     <th>Chesse Merit $</th>
	      <td>'.$infoproducto["chessemerit$"].'</td>
     

	</tr>

		<tr>
		<th>Protein</th>
		<td>'.$infoproducto["protein"].'</td>
		<td>'.$infoproducto["sumaryprotein"].'</td>
		 <th>Grazing Merit  $</th>
		 <td>'.$infoproducto["grazingmerit$"].'</td>
		
	</tr>



	</tr>

		<tr>
		<th>SCS</th>
		<td>'.$infoproducto["scs"].'</td>
		<td>'.$infoproducto["sumaryscs"].'</td>
		 <th>Gestation len.</th>
		 <td>'.$infoproducto["gestationlen"].'</td>
	</tr>



	</tr>

		<tr>
		<th>PL</th>
		<td>'.$infoproducto["PL"].'</td>
		<td>'.$infoproducto["plsumary"].'</td>
		 <th>Fert. Index</th>
		 <td>'.$infoproducto["fertindex"].'</td>
	</tr>


	</tr>

		<tr>
		<th>DPR</th>
		<td>'.$infoproducto["DPR"].'</td>
		<td>'.$infoproducto["dprsumary"].'</td>
        <th>Livability</th>
        <td>'.$infoproducto["livability"].'</td>


	</tr>

		<tr>
		<th>HCR</th>
		<td>'.$infoproducto["hcr"].'</td>
		<td></td>

		<th >Rel</th>
		<th >EFI</th>
	

	</tr>

	</tr>

		<tr>
		<th>CCR</th>
		<td>'.$infoproducto["ccr"].'</td>
		<td></td>
			<td>'.$infoproducto["REL"].'</td>
			<td>'.$infoproducto["EFI"].'</td>
	
	</tr>
	</tbody>
</table>


<br><br>



  <table>
	<thead>
	<tr>
		<th>Rasgo</th>
		<th>Lineal</th>
				<th style= "background-color: #ff3333">Tendencia</th> 
		<th colspan="2">AJCA TYPE SUMMARY</th>
		<th style= "background-color: #ff3333">Tendencia</th> 


	
	</tr>
	</thead>
	<tbody>
	<tr>
		<th>Estatura</th>
		<td>'.$infoproducto["estatura"].'</td>
           <th>small</td>
		<td></td>
		<td></td>
		<th>Tall</th>

      

     

	</tr>
	<tr>
		<th>fortaleza</th>
		<td>'.$infoproducto["fuerza"].'</th>
	    <th>Estrecho</td>
		<td></td>
		<td></td>
		<th>Ancho</th>


	</tr>

		<tr>
		<th>Profundidad del Cuerpo</th>
		<td>'.$infoproducto["profundbody"].'</td>
		   <th>Superficial</td>
		<td></td>
		<td></td>
		<th> Profundo</th>

		
	</tr>



	</tr>

		<tr>
		<th>forma lechera</th>
		<td>'.$infoproducto["dairyform"].'</td>
		   <th>Costilla Cerrada</td>
		<td></td>
		<td></td>
		<th>Costilla abierta</th>
	
	</tr>



	</tr>

		<tr>
		<th>Angulo Grupa</th>
		<td>'.$infoproducto["angulogrupa"].'</td>
		   <th>ascendiendo</td>
		<td></td>
		<td></td>
		<th>inclinado</th>
	
	</tr>


	</tr>

		<tr>
		<th>Ancho de grupa</th>
		<td>'.$infoproducto["anchogrupa"].'</td>
		   <th>estrecho</td>
		<td></td>
		<td></td>
		<th>amplio</th>
	


	</tr>

		<tr>
		<th>Patas  traseras vista Lateral</th>
		<td>'.$infoproducto["patastraseraslateral"].'</td>
		   <th>Posty</td>
		<td></td>
		<td></td>
		<th>Derecho</th>
		

	

	</tr>
    <th>conjunto de patas traseras vista trasera</th>
		<td>'.$infoproducto["conjunto de patas vistatrasera"].'</td>
		   <th>pezuñas abertas</td>
		<td></td>
		<td></td>
		<th>paralelao</th>
	
	</tr>

		<tr>
		<th>Ang. Pezuña</th>
		<td>'.$infoproducto["ang pezuña"].'</td>
		   <th>Angulo bajo</td>
		<td></td>
		<td></td>
		<th> escarpado</th>
	
	    


     



	
	</tr>
 <th> altura de la ubre trasera</th>
		<td>'.$infoproducto["altura ubre trasera"].'</td>
		   <th>bajo</td>
		<td></td>
		<td></td>
		<th>alto</th>

	</tr>



	</tr>
 <th>Colocación de pezones (frente)</th>
		<td>'.$infoproducto["col pezones frente"].'</td>
		   <th>wide</td>
		<td></td>
		<td></td>
		<th>close</th>

	</tr>



	</tr>
 <th> 

Colocación de pezones (trasero)</th>
		<td>'.$infoproducto["col pezones trasero"].'</td>
		   <th>wide</td>
		<td></td>
		<td></td>
		<th>close</th>

	</tr>


	</tr>
 <th> profundidad de la ubre</th>
		<td>'.$infoproducto["profundubre"].'</td>
		   <th>Profundo</td>
		<td></td>
		<td></td>
		<th>Superficial</th>

	</tr>




		</tr>
 <th> Col. pez . ant</th>
		<td>'.$infoproducto["col pez ant"].'</td>
		   <th>cerrado</td>
		<td></td>
		<td></td>
		<th>abierto</th>

	</tr>


	</tr>
 <th> Col. pez . post</th>
		<td>'.$infoproducto["col pez post"].'</td>
		   <th>cerrado</td>
		<td></td>
		<td></td>
		<th>abierto</th>

	</tr>


	</tr>
 <th> Largo pez.</th>
		<td>'.$infoproducto["largopez"].'</td>
		   <th>corto</td>
		<td></td>
		<td></td>
		<th>largo</th>

	</tr>




	</tbody>
</table>



<br>br>



<div id="container" style="min-width: 310px; max-width: 800px; height: 400px; margin: 0 auto"></div>


';

}else{








echo'
<div class="col-xs-12">



<style>
table {
  border-collapse: collapse;
  width: 15%;
}

th, td {
  padding: 8px;
  text-align: left;
  border-bottom: 1px solid #ddd;
}

tr:hover {background-color:#f5f5f5;}
</style>
</head>
<body>

<h2>INDICE GENENETICO DE </h2>
<p>Move the mouse over the table rows to see the effect.</p>

<table>
  <tr>

  </tr>
  <tr>
    <th>IMQ</th>
    <td>118.00</td>

  </tr>
  <tr>
    <th>Leche</th>
    <td>+116.00</td>

  </tr>
  <tr>
    <th>G kg</th>
    <td>+34.00</td>

  </tr>
  <tr>
    <th>G %</th>
    <td>118.00</td>

  </tr>
    <tr>
    <th>P kg</th>
    <td></td>

  </tr>

    <tr>
    <th>P %</th>
    <td></td>
    
  </tr>
</table>

</body>


';




	}

			?>







<br><br>
				<!--=====================================
				BOTONES DE COMPRA
				======================================-->

				<div class="row botonesCompra">

				<?php






					if($infoproducto["precio"]==0){

						echo '<div class="col-md-6 col-xs-12">';

						if(isset($_SESSION["validarSesion"]) && $_SESSION["validarSesion"] == "ok"){

							if($infoproducto["tipo"]=="virtual"){
						
								echo '<button class="btn btn-default btn-block btn-lg backColor agregarGratis" idProducto="'.$infoproducto["id"].'" idUsuario="'.$_SESSION["id"].'" tipo="'.$infoproducto["tipo"].'" titulo="'.$infoproducto["titulo"].'">SOLICITAR INFORMES</button>

								<br>

									<div class="col-xs-12 panel panel-info text-left">

									<strong>¡Atención!</strong>

										Al selecionar solicitar informes usted podra proporcionarnos sus datos de contacto para que unos de nuestros ejecutivos puedan darle mas informacion o realizar la compra.
										
										<strong>NOTA</strong>
                                           Podra dar comentarios en mi perfil en la pestaña CONTACTANO.
									</div>';






							}else{

								echo '<button class="btn btn-default btn-block btn-lg backColor agregarGratis" idProducto="'.$infoproducto["id"].'" idUsuario="'.$_SESSION["id"].'" tipo="'.$infoproducto["tipo"].'" titulo="'.$infoproducto["titulo"].'">SOLICITAR INFORMES</button>

									<br>

									<div class="col-xs-12 panel panel-info text-left">

									<strong>¡Atención!</strong>

										Al selecionar solicitar informes usted podra proporcionarnos sus datos de contacto para que unos de nuestros ejecutivos puedan darle mas informacion o realizar la compra.
										
										<strong>NOTA</strong>
                                           Podra dar comentarios en mi perfil en la pestaña CONTACTANOS.
									</div>








                           










								';

							}

						}else{

							echo '<a href="#modalIngreso" data-toggle="modal">

								<button class="btn btn-default btn-block btn-lg backColor">	SOLICITAR AHORA</button>

							</a>';

						}

						echo '</div>';

					}else{

						if($infoproducto["tipo"]=="virtual"){

							echo '<div class="col-md-6 col-xs-12">';

							if(isset($_SESSION["validarSesion"])){

								if($_SESSION["validarSesion"] == "ok"){

									echo '<a id="btnCheckout" href="#modalComprarAhora" data-toggle="modal" idUsuario="'.$_SESSION["id"].'"><button class="btn btn-default btn-block btn-lg">
									<small>COMPRAR AHORA</small></button></a>';

								}



							}else{

								echo '<a href="#modalIngreso" data-toggle="modal"><button class="btn btn-default btn-block btn-lg">
									<small>COMPRAR AHORA</small></button></a>';
			
							}

							echo '</div>

								<div class="col-md-6 col-xs-12">';

								if($infoproducto["oferta"] != 0){
									
									echo '<button class="btn btn-default btn-block btn-lg backColor agregarCarrito"  idProducto="'.$infoproducto["id"].'" imagen="'.$servidor.$infoproducto["portada"].'" titulo="'.$infoproducto["titulo"].'" precio="'.$infoproducto["precioOferta"].'" tipo="'.$infoproducto["tipo"].'" peso="'.$infoproducto["peso"].'">';

								}else{

									echo '<button class="btn btn-default btn-block btn-lg backColor agregarCarrito"  idProducto="'.$infoproducto["id"].'" imagen="'.$servidor.$infoproducto["portada"].'" titulo="'.$infoproducto["titulo"].'" precio="'.$infoproducto["precio"].'" tipo="'.$infoproducto["tipo"].'" peso="'.$infoproducto["peso"].'">';


								}

								echo   '<small>ADICIONAR AL CARRITO</small> 

									<i class="fa fa-shopping-cart col-md-0"></i>



									</button>

								</div>';
						}else{

							echo '<div class="col-lg-6 col-md-8 col-xs-12">';

							if($infoproducto["oferta"] != 0){
									
									echo '<button class="btn btn-default btn-block btn-lg backColor agregarCarrito"  idProducto="'.$infoproducto["id"].'" imagen="'.$servidor.$infoproducto["portada"].'" titulo="'.$infoproducto["titulo"].'" precio="'.$infoproducto["precioOferta"].'" tipo="'.$infoproducto["tipo"].'" peso="'.$infoproducto["peso"].'">';

								}else{

									echo '<button class="btn btn-default btn-block btn-lg backColor agregarCarrito"  idProducto="'.$infoproducto["id"].'" imagen="'.$servidor.$infoproducto["portada"].'" titulo="'.$infoproducto["titulo"].'" precio="'.$infoproducto["precio"].'" tipo="'.$infoproducto["tipo"].'" peso="'.$infoproducto["peso"].'">';

								}


									echo 'ADICIONAR AL CARRITO 

									<i class="fa fa-shopping-cart"></i>

									</button>

								</div>';

						}

					}

				?>

				</div>
				








		<!--=====================================
		COMENTARIOS
		======================================-->

		<br>

		<div class="row">

		
					
				</a></li>

			</ul>

			<br>

		</div>

		<div class="row comentarios">


		</div>

	

	</div>

</div>

<!--=====================================
ARTÏCULOS RELACIONADOS
======================================-->
<div class="container-fluid productos">
	
	<div class="container">

		<div class="row">

			<div class="col-xs-12 tituloDestacado">

				<div class="col-sm-6 col-xs-12">
			
					<h1><small>PRODUCTOS RELACIONADOS</small></h1>

				</div>

				<div class="col-sm-6 col-xs-12">

				<?php

					$item = "id";
					$valor = $infoproducto["id_subcategoria"];

					$rutaArticulosDestacados = ControladorProductos::ctrMostrarSubcategorias($item, $valor);

					echo '<a href="'.$url.$rutaArticulosDestacados[0]["ruta"].'">
						
						<button class="btn btn-default backColor pull-right">
							
							VER MÁS <span class="fa fa-chevron-right"></span>

						</button>

					</a>';

				?>

				</div>

			</div>

			<div class="clearfix"></div>



		</div>

		<?php

			$ordenar = "";
			$item = "id_subcategoria";
			$valor = $infoproducto["id_subcategoria"];
			$base = 0;
			$tope = 4;
			$modo = "Rand()";

			$relacionados = ControladorProductos::ctrMostrarProductos($ordenar, $item, $valor, $base, $tope, $modo);

			if(!$relacionados){

				echo '<div class="col-xs-12 error404">

					<h1><small>¡Oops!</small></h1>

					<h2>No hay productos relacionados</h2>

				</div>';

			}else{

				echo '<ul class="grid0">';

				foreach ($relacionados as $key => $value) {

				if($value["estado"] != 0){
				
					echo '<li class="col-md-3 col-sm-6 col-xs-12">

						<figure>
							
							<a href="'.$url.$value["ruta"].'" class="pixelProducto">
								
								<img src="'.$servidor.$value["portada"].'" class="img-responsive">

							</a>

						</figure>

						'.$value["id"].'

						<h4>
				
							<small>
								
								<a href="'.$url.$value["ruta"].'" class="pixelProducto">
									
									'.$value["titulo"].'<br>

									<span style="color:rgba(0,0,0,0)">-</span>';

									$fecha = date('Y-m-d');
									$fechaActual = strtotime('-30 day', strtotime($fecha));
									$fechaNueva = date('Y-m-d', $fechaActual);

									if($fechaNueva < $value["fecha"]){

										echo '<span class="label label-warning fontSize">Nuevo</span> ';

									}

									if($value["oferta"] != 0 && $value["precio"] != 0){

										echo '<span class="label label-warning fontSize">'.$value["descuentoOferta"].'% off</span>';

									}

								echo '</a>	

							</small>			

						</h4>

						<div class="col-xs-6 precio">';

						if($value["precio"] == 0){

							echo '<h2><small></small></h2>';

						}else{

							if($value["oferta"] != 0){

								echo '<h2>

										<small>
					
											<strong class="oferta">USD $'.$value["precio"].'</strong>

										</small>

										<small>$'.$value["precioOferta"].'</small>
									
									</h2>';

							}else{

								echo '<h2><small>USD $'.$value["precio"].'</small></h2>';

							}
							
						}
										
						echo '</div>

						<div class="col-xs-6 enlaces">
							
							<div class="btn-group pull-right">
								
								<button type="button" class="btn btn-default btn-xs deseos" idProducto="'.$value["id"].'" data-toggle="tooltip" title="Agregar y comparar a mi lista de deseos">
									
									<i class="fa fa-star" aria-hidden="true"></i>

								</button>';

								if($value["tipo"] == "virtual" && $value["precio"] != 0){

									if($value["oferta"] != 0){

										echo '<button type="button" class="btn btn-default btn-xs agregarCarrito"  idProducto="'.$value["id"].'" imagen="'.$servidor.$value["portada"].'" titulo="'.$value["titulo"].'" precio="'.$value["precioOferta"].'" tipo="'.$value["tipo"].'" peso="'.$value["peso"].'" data-toggle="tooltip" title="Agregar al carrito de compras">

										<i class="fa fa-shopping-cart" aria-hidden="true"></i>

										</button>';

									}else{

										echo '<button type="button" class="btn btn-default btn-xs agregarCarrito"  idProducto="'.$value["id"].'" imagen="'.$servidor.$value["portada"].'" titulo="'.$value["titulo"].'" precio="'.$value["precio"].'" tipo="'.$value["tipo"].'" peso="'.$value["peso"].'" data-toggle="tooltip" title="Agregar al carrito de compras">

										<i class="fa fa-shopping-cart" aria-hidden="true"></i>

										</button>';

									}

								}

								echo '<a href="'.$url.$value["ruta"].'" class="pixelProducto">
								
									<button type="button" class="btn btn-default btn-xs" data-toggle="tooltip" title="Ver producto">
										
										<i class="fa fa-eye" aria-hidden="true"></i>

									</button>	
								
								</a>

							</div>

						</div>

					</li>';

				}
			}

			echo '</ul>';

		}

		?>

	</div>

</div>

<!--=====================================
VENTANA MODAL PARA CHECKOUT
======================================-->

<div id="modalComprarAhora" class="modal fade modalFormulario" role="dialog">
	
	 <div class="modal-content modal-dialog">
	 	
		<div class="modal-body modalTitulo">
			
			<h3 class="backColor">REALIZAR PAGO</h3>

			<button type="button" class="close" data-dismiss="modal">&times;</button>

			<div class="contenidoCheckout">

				<?php

				$respuesta = ControladorCarrito::ctrMostrarTarifas();

				echo '<input type="hidden" id="tasaImpuesto" value="'.$respuesta["impuesto"].'">
					  <input type="hidden" id="envioNacional" value="'.$respuesta["envioNacional"].'">
				      <input type="hidden" id="envioInternacional" value="'.$respuesta["envioInternacional"].'">
				      <input type="hidden" id="tasaMinimaNal" value="'.$respuesta["tasaMinimaNal"].'">
				      <input type="hidden" id="tasaMinimaInt" value="'.$respuesta["tasaMinimaInt"].'">
				      <input type="hidden" id="tasaPais" value="'.$respuesta["pais"].'">

				';

				?>
				
				<div class="formEnvio row">
					
					<h4 class="text-center well text-muted text-uppercase">Información de envío</h4>

					<div class="col-xs-12 seleccionePais">
						
						

					</div>

				</div>

				<br>

				<div class="formaPago row">
					
					<h4 class="text-center well text-muted text-uppercase">Elige la forma de pago</h4>

					<figure class="col-xs-6">
						
						<center>
							
							<input id="checkPaypal" type="radio" name="pago" value="paypal" checked>

						</center>	
						
						<img src="<?php echo $url; ?>vistas/img/plantilla/paypal.jpg" class="img-thumbnail">		

					</figure>

					<figure class="col-xs-6">
						
						<center>
							
							<input id="checkPayu" type="radio" name="pago" value="payu">

						</center>

						<img src="<?php echo $url; ?>vistas/img/plantilla/payu.jpg" class="img-thumbnail">

					</figure>

				</div>

				<br>

				<div class="listaProductos row">
					
					<h4 class="text-center well text-muted text-uppercase">Productos a comprar</h4>

					<table class="table table-striped tablaProductos">
						
						 <thead>
						 	
							<tr>		
								<th>Producto</th>
								<th>Cantidad</th>
								<th>Precio</th>
							</tr>

						 </thead>

						 <tbody>
						 	


						 </tbody>

					</table>

					<div class="col-sm-6 col-xs-12 pull-right">
						
						<table class="table table-striped tablaTasas">
							
							<tbody>
								
								<tr>
									<td>Subtotal</td>	
									<td><span class="cambioDivisa">USD</span> $<span class="valorSubtotal" valor="0">0</span></td>	
								</tr>

								<tr>
									<td>Envío</td>	
									<td><span class="cambioDivisa">USD</span> $<span class="valorTotalEnvio" valor="0">0</span></td>	
								</tr>

								<tr>
									<td>Impuesto</td>	
									<td><span class="cambioDivisa">USD</span> $<span class="valorTotalImpuesto" valor="0">0</span></td>	
								</tr>

								<tr>
									<td><strong>Total</strong></td>	
									<td><strong><span class="cambioDivisa">USD</span> $<span class="valorTotalCompra" valor="0">0</span></strong></td>	
								</tr>

							</tbody>	

						</table>

						 <div class="divisa">

						 	<select class="form-control" id="cambiarDivisa" name="divisa">
						 		
							

						 	</select>	

						 	<br>

						 </div>

					</div>

					<div class="clearfix"></div>

					<form class="formPayu" style="display:none">
					 
						<input name="merchantId" type="hidden" value=""/>
						<input name="accountId" type="hidden" value=""/>
						<input name="description" type="hidden" value=""/>
						<input name="referenceCode" type="hidden" value=""/>	
						<input name="amount" type="hidden" value=""/>
						<input name="tax" type="hidden" value=""/>
						<input name="taxReturnBase" type="hidden" value=""/>
						<input name="shipmentValue" type="hidden" value=""/>
						<input name="currency" type="hidden" value=""/>
						<input name="lng" type="hidden" value="es"/>
						<input name="confirmationUrl" type="hidden" value="" />
						<input name="responseUrl" type="hidden" value=""/>
						<input name="declinedResponseUrl" type="hidden" value=""/>
						<input name="displayShippingInformation" type="hidden" value=""/>
						<input name="test" type="hidden" value="" />
						<input name="signature" type="hidden" value=""/>

					  <input name="Submit" class="btn btn-block btn-lg btn-default backColor" type="submit"  value="PAGAR" >
					</form>
					
					<button class="btn btn-block btn-lg btn-default backColor btnPagar">PAGAR</button>

				</div>

			</div>

		</div>

		<div class="modal-footer">
      	
      	</div>

	</div>

</div>


<?php

if($infoproducto["tipo"] == "virtual"){

	echo '<script type="application/ld+json">

			{
			  "@context": "http://schema.org/",
			  "@type": "Product",
			  "name": "'.$infoproducto["titulo"].'",
			  "image": [';

			  for($i = 0; $i < count($multimedia); $i ++){

			  	echo $servidor.$multimedia[$i]["foto"].',';

			  }
			
			  echo '],
			  "description": "'.$infoproducto["descripcion"].'"
			  "code": "'.$infoproducto["code"].'"

	  
			}



		</script>';

}else{

	echo '<script type="application/ld+json">

			{
			  "@context": "http://schema.org",
			  "@type": "Course",
			  "name": "'.$infoproducto["titulo"].'",
			  "description": "'.$infoproducto["descripcion"].'",
			


			  "provider": {
			    "@type": "Organization",
			    "name": "Tu Logo",
			    "sameAs": "'.$url.$infoproducto["ruta"].'"
			  }
			}

		</script>';

}

?>
